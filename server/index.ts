import { expressMiddleware } from '@apollo/server/express4';
import express, { Express } from 'express';
import * as http from 'http';
// import * as socketio from 'socket.io';
import next, { NextApiHandler, NextApiRequest } from 'next';
import { apolloServer } from './graphql';
import { init } from './middlewares';

const port: number = parseInt(process.env.PORT || '3001', 10);
const dev: boolean = process.env.NODE_ENV !== 'production';
const nextApp = next({ dev });
const nextHandler: NextApiHandler = nextApp.getRequestHandler();

nextApp.prepare().then(async () => {
  const app: Express = express();
  const server: http.Server = http.createServer(app);
  // const io: socketio.Server = new socketio.Server();
  // io.attach(server, {
  //   cors: {
  //     origin: '*',
  //     methods: ['GET', 'POST'],
  //   },
  // });

  init(app);

  const serverApollo = apolloServer(server);

  await serverApollo.start();

  app.use(
    '/api/graphql',
    // expressMiddleware accepts the same arguments:
    // an Apollo Server instance and optional configuration options
    expressMiddleware(serverApollo, {
      context: async ({ req, res }) => ({
        res: res,
        req: req,
        token: req.headers.token,
      }),
    }),
  );

  // io.on('connection', (socket: socketio.Socket) => {
  //   console.info('user connected');
  //   console.info(socket.id);
  //   socket.emit('status', 'Hello from Socket.io');

  //   socket.on('disconnect', () => {
  //     console.info('client disconnected');
  //   });
  // });

  app.all('*', (req: NextApiRequest, res: any) => {
    return nextHandler(req, res);
  });

  await new Promise<void>((resolve) => server.listen({ port }, resolve));

  console.info(`🚀 Server ready at http://localhost:${port}`);
});
