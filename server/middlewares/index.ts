import { Express } from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';

export const init = (server: Express) => {
  const allowedOrigins = [
    `http://localhost:3001`,
    `http://localhost:5002`,
    `https://studio.apollographql.com`,
    `http://front.pay4me.online`,
    `http://front.pay4me.online:3001`,
  ];

  server.use(
    cors<cors.CorsRequest>({
      origin: function (origin, callback) {
        // allow requests with no origin
        // (like mobile apps or curl requests)
        if (!origin) return callback(null, true);
        if (allowedOrigins.indexOf(origin) === -1) {
          const msg =
            'The CORS policy for this site does not ' +
            'allow access from the specified Origin.';
          return callback(new Error(msg), false);
        }
        return callback(null, true);
      },
      credentials: true,
    }),
    bodyParser.json(),
  );

  server.use(bodyParser.urlencoded({ extended: false }));
};
