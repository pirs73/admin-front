import { gql } from '@apollo/client';
import {
  adBanksTypes,
  adDetailsTypes,
  adsTypes,
  apiTokensTypes,
  authTypes,
  balancesTypes,
  cashbacksTypes,
  currenciesTypes,
  currencyPairsTypes,
  logsTypes,
  merchantsTypes,
  referralsType,
  tradesType,
  transactionsTypes,
  usersTypes,
  walletsTypes,
} from './types';

const params = `
  page: Int
  itemsPerPage: Int
  pagination: Boolean
`;

export const typeDefs = gql`
  ${adBanksTypes}
  ${adDetailsTypes}
  ${adsTypes}
  ${apiTokensTypes}
  ${authTypes}
  ${balancesTypes}
  ${cashbacksTypes}
  ${currenciesTypes}
  ${currencyPairsTypes}
  ${logsTypes}
  ${merchantsTypes}
  ${referralsType}
  ${tradesType}
  ${transactionsTypes}
  ${usersTypes}
  ${walletsTypes}

  type Query {
    adsList(
      ${params}
      isActive: Boolean
      currencyPair: String
      currencyPairs: [String]
      bank: String
      banks: [String]
      user: String
      users: [String]
    ): AdsResponse!

    getAdResource(uuid: String!): AdsListItem!

    adBanks(pagination: Boolean): AdBanksResponse!

    adDetails(
      ${params}
    ): AdDetailsResponse!

    apiTokens(
      ${params}
    ): ApiTokensResponse!

    balances(
      ${params}
    ): BalancesResponse!

    cashbacks(
      ${params}
    ): CashbacksResponse!

    currencies(
      ${params}
    ): CurrenciesResponse!

    currencyPairs(
      ${params}
    ): CurrencyPairsResponse!

    logs(
      ${params}
      createdAtBefore: String
      createdAtStrictlyBefore: String
      createdAtAfter: String
      createdAtStrictlyAfter: String
      channel: String
      channels: [String]
      level: String
      levels: [String]
      belongToUser: String
      belongToUsers: [String]
    ): LogsResponse!

    merchants(
      ${params}
    ): MerchantsResponse!

    referrals(
      ${params}
    ): ReferralsResponse!

    trades(
      ${params}
      order: CreatedAt
      isCanceled: Boolean
      isPaymentSend: Boolean
      isOnDispute: Boolean
      isConfirmed: Boolean
    ): TradesResponse!

    transactions(
      ${params}
    ): TransactionsResponse!

    user(id: ID!): UsersItem!

    users(
      ${params}
      isOnline: Boolean
      isActivated: Boolean
      isTrader: Boolean
      isMerchant: Boolean
      createdAtBefore: String
      createdAtStrictlyBefore: String
      createdAtAfter: String
      createdAtStrictlyAfter: String
      lastOnlineAtBefore: String
      lastOnlineAtStrictlyBefore: String
      lastOnlineAtAfter: String
      lastOnlineAtStrictlyAfter: String
      username: String
      usernames: [String]
    ): UsersResponse!

    wallets(
      ${params}
      address: String
      addresses: [String]
      user: String
      users: [String]
    ): WalletsResponse!

    viewer: ViewerResonse!
  }

  type Mutation {
    signUp(input: SignUpInput!): Boolean!

    signIn(input: SignInInput!): SignInPayload!

    signOut: Boolean!

    refreshToken: RefreshTokenPayload

    checkAccessToken: Boolean!
  }
`;
