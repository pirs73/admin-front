export const currenciesTypes = `
  input GetCurrenciesParams {
    page: Int
    itemsPerPage: Int
    pagination: Boolean
  }

  type CurrenciesItem {
    id: String!
    name: String!
    symbol: String!
    icon: String
    isFiat: Boolean!
  }

  type CurrenciesResponse {
    totalItems: Int!
    currenciesItems: [CurrenciesItem]!
  }
`;
