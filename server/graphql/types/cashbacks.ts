export const cashbacksTypes = `
  input GetCashbacksParams {
    page: Int
    itemsPerPage: Int
    pagination: Boolean
  }

  type CashbacksItem {
    id: Int!
    cashbackToUser: String!
    cashbackFromMerchant: String
    cashbackFromUser: String
    cashbackSellCrypto: String!
    cashbackBuyCrypto: String!
  }
  type CashbacksResponse {
    totalItems: Int!
    cashbacksItems: [CashbacksItem]!
  }
`;
