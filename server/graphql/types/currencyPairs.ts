export const currencyPairsTypes = `
  input GetCurrencyPairsParams {
    page: Int
    itemsPerPage: Int
    pagination: Boolean
  }

  type CurrencyPairsItem {
    name: String!
    currencyFrom: String!
    currencyTo: String!
    exchangeRate: String
  }

  type CurrencyPairsResponse {
    totalItems: Int!
    currencyPairsItems: [CurrencyPairsItem]!
  }
`;
