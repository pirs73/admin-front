export const walletsTypes = `
  input GetWalletsParams {
    page: Int
    itemsPerPage: Int
    pagination: Boolean
    address: String
    addresses: [String]
    user: String
    users: [String]
  }

  type WalletsUser {
    username: String!
  }

  type WalletsCurrency {
    id: String!
    name: String!
  }

  type WalletsItem {
    id: Int!
    address: String!
    user: WalletsUser
    currency: WalletsCurrency!
  }

  type WalletsResponse {
    totalItems: Int!
    walletsItems: [WalletsItem]!
  }
`;
