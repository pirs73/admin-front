export const apiTokensTypes = `
  input GetApiTokensParams {
    page: Int
    itemsPerPage: Int
    pagination: Boolean
  }

  type ApiTokensItem {
    id: String!
    privateKey: String
    isOnline: Boolean!
    name: String!
    createdAt: String!
    lastUsedAt: String
    belongToUser: String!
  }

  type ApiTokensResponse {
    totalItems: Int!
    apiTokensItems: [ApiTokensItem]!
  }
`;
