export const authTypes = `
  input SignUpInput {
    username: String!
    password: String!
    referralCode: String!
  }

  input SignInInput {
    username: String!
    password: String!
  }

  type SignInPayload {
    token: String
  }

  type RefreshTokenPayload {
    jwt_token: String
  }
`;
