export const adBanksTypes = `
  input GetAdBanksParams {
    pagination: Boolean
  }

  type AdBanksItem {
    name: String!
    showOrder: Int!
    shortName: String!
  }

  type AdBanksResponse {
    totalItems: Int!
    adBanksItems: [AdBanksItem]!
  }
`;
