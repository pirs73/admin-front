export const usersTypes = `
    enum OrderId {
      ASC
      DESC
    }

   input GetUsersParams {
    page: Int
    itemsPerPage: Int
    pagination: Boolean
    isOnline: Boolean
    isActivated: Boolean
    isTrader: Boolean
    isMerchant: Boolean
    createdAtBefore: String
    createdAtStrictlyBefore: String
    createdAtAfter: String
    createdAtStrictlyAfter: String
    lastOnlineAtBefore: String
    lastOnlineAtStrictlyBefore: String
    lastOnlineAtAfter: String
    lastOnlineAtStrictlyAfter: String
    username: String
    usernames: [String]
  }

  type UsersItem {
    username: String!
    phone: String
    referredBy: String
    allowedNumberOfReferrals: Int
    referral: String
    createdAt: String!
    lastOnlineAt: String
    isOnline: Boolean!
    isActivated: Boolean!
    isTrader: Boolean!
    isMerchant: Boolean!
    scopes: [String]
  }

  type UsersResponse {
    totalItems: Int!
    usersItems: [UsersItem]!
  }

  type ViewerResonse {
    viewer: UsersItem!
    token: String!
  }
`;
