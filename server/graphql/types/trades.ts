export const tradesType = `
  enum CreatedAt {
    asc
    desc
  }

  input GetTradesParams {
    page: Int
    itemsPerPage: Int
    pagination: Boolean
    order: CreatedAt
    isCanceled: Boolean
    isPaymentSend: Boolean
    isOnDispute: Boolean
    isConfirmed: Boolean
  }

  type TradesItem {
    uuid: String
    currencyPair: String!
    amount: String!
    rate: String!
    createdAt: String!
    completedAt: String
    user: String
    adDetail: String
    isConfirmed: Boolean!
    isOnDispute: Boolean!
    isPaymentSend: Boolean!
    isCanceled: Boolean!
  }

  type TradesResponse {
    totalItems: Int!
    tradesItems: [TradesItem]!
  }
`;
