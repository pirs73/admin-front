export const transactionsTypes = `
   input GetTransactionsParams {
    page: Int
    itemsPerPage: Int
    pagination: Boolean
  }

  type TransferToUser {
    username: String!
  }

  type TransactionsItem {
    uuid: String
    transferToUser: TransferToUser
    amount: String!
    trade: String
    currency: String!
    wallet: String
    txId: String
    externalWallet: String
    isFinalized: Boolean!
  }

  type TransactionsResponse {
    totalItems: Int!
    transactionsItems: [TransactionsItem]!
  }
`;
