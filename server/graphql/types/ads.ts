export const adsTypes = `
  input GetAdsParams {
    page: Int
    itemsPerPage: Int
    pagination: Boolean
    isActive: Boolean
    currencyPair: String
    currencyPairs: [String]
    bank: String
    banks: [String]
    user: String
    users: [String]
  }

  type CurrencyPair {
    name: String!
  }

  type DetailsItem {
    uuid: String
    number: String!
    text: String
    transactionSum: String!
  }

  type Bank {
    name: String!
    shortName: String!
  }

  type AdsListItem {
    uuid: String
    currencyPair: CurrencyPair!
    minAmount: String!
    maxAmount: String!
    user: String
    details: [DetailsItem!]!
    isActive: Boolean!
    bank: Bank!
  }

  type AdsResponse {
    totalItems: Int!
    adsItems: [AdsListItem]!
  }

  input GetAdResourceParams {
    uuid: String!
  }
`;
