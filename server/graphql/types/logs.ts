export const logsTypes = `
  input GetLogsParams {
    page: Int
    itemsPerPage: Int
    pagination: Boolean
    createdAtBefore: String
    createdAtStrictlyBefore: String
    createdAtAfter: String
    createdAtStrictlyAfter: String
    channel: String
    channels: [String]
    level: Int
    levels: [Int]
    belongToUser: String
    belongToUsers: [String]
  }

  type LogsBinanceSearchParams {
    proMerchantAds: Boolean
    page: Int
    rows: Int
    payTypes: [String]
    countries: [String]
    publisherType: String
    fiat: String
    tradeType: String
    asset: String
    merchantCheck: Boolean
  }

  type LogsContext {
    exception: [String]
    binanceURL: String
    binanceAverageRateMinLiquidity: Float
    binanceSearchParams: LogsBinanceSearchParams
    average_rate: String
    rate: Float
    liquidity: Float
  }

  type LogsItem {
    id: Int!
    channel: String!
    level: String!
    message: String!
    context: LogsContext!
    belongToUser: String
    extra: [String]!
    createdAt: String!
  }

  type LogsResponse {
    totalItems: Int!
    logsItems: [LogsItem]!
  }
`;
