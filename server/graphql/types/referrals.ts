export const referralsType = `
  input GetReferralsParams {
    page: Int
    itemsPerPage: Int
    pagination: Boolean
  }

  type ReferralsItem {
    uuid: String
    expireAt: String
    createdAt: String!
    isMultipleRegistrationsAllowed: Boolean!
    isUsed: Boolean!
  }

  type ReferralsResponse {
    totalItems: Int!
    referralsItems: [ReferralsItem]!
  }
`;
