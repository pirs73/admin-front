export const adDetailsTypes = `
  input GetAdDetailsParams {
    page: Int
    itemsPerPage: Int
    pagination: Boolean
  }

  type AdDetailItem {
    uuid: ID!
    ad: String!
    number: String!
    text: String!
    transactionSum: String!
  }

  type AdDetailsResponse {
    totalItems: Int!
    adDetailsItems: [AdDetailItem]!
  }
`;
