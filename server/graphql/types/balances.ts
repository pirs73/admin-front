export const balancesTypes = `
  input GetBalancesParams {
    page: Int
    itemsPerPage: Int
    pagination: Boolean
  }

  type BalancesItem {
    id: String!
    currency: String!
    user: String!
    balance: String
    onHold: String
  }

  type BalancesResponse {
    totalItems: Int!
    balancesItems: [BalancesItem]!
  }
`;
