export const merchantsTypes = `
  input GetMerchantsParams {
    page: Int
    itemsPerPage: Int
    pagination: Boolean
  }

  type MerchantsItem {
    uuid: String
    name: String!
    user: String!
    discountSellCrypto: String!
    discountBuyCrypto: String!
    isAllowed: Boolean!
    isActive: Boolean!
    createdAt: String!
  }

  type MerchantsResponse {
    totalItems: Int!
    merchantsItems: [MerchantsItem]!
  }
`;
