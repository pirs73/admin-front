import { GraphQLError } from 'graphql';
import fetch from 'isomorphic-unfetch';
import { parseCookies } from 'nookies';
import { API_URL } from '../../../constants';
import {
  GetAdDetailsInputParams,
  AdDetailsResponse,
} from '../../../interfaces';

export const adDetailsQuery = {
  adDetails: async (
    _root: undefined,
    { page, itemsPerPage, pagination }: GetAdDetailsInputParams,
    context,
    _info,
  ): Promise<AdDetailsResponse | null | undefined> => {
    const req = context.req;
    const parsedCookies = parseCookies({ req });

    if (!parsedCookies['jwt_token']) {
      return null;
    }

    try {
      const platformRes = await fetch(
        `${API_URL}/ad_details?page=${page}&itemsPerPage=${itemsPerPage}&pagination=${pagination}`,
        {
          method: 'GET',
          headers: {
            Accept: 'application/ld+json',
            'Content-Type': 'application/ld+json',
            Authorization: `Bearer ${parsedCookies['jwt_token']}`,
          },
        },
      );

      if (platformRes.ok) {
        const text = await platformRes.text();
        const json = JSON.parse(text);
        const totalItems = json['hydra:totalItems'];
        const adDetailsItems = json['hydra:member'].map((el) => {
          el.ad = el.ad ? el.ad.split('/').reverse()[0] : '';

          return { ...el };
        });

        return {
          totalItems,
          adDetailsItems,
        };
      }
    } catch (error) {
      throw new GraphQLError(`GraphQLError ${error}`);
    }
  },
};
