import { GraphQLError } from 'graphql';
import fetch from 'isomorphic-unfetch';
import { parseCookies } from 'nookies';
import { parseJSON, format } from 'date-fns';
import { API_URL } from '../../../constants';
import { GetReferralsParams, ReferralsResponse } from '../../../interfaces';

export const referralsQuery = {
  referrals: async (
    _root: undefined,
    { page, itemsPerPage, pagination }: GetReferralsParams,
    context,
    _info,
  ): Promise<ReferralsResponse | null | undefined> => {
    const req = context.req;
    const parsedCookies = parseCookies({ req });

    if (!parsedCookies['jwt_token']) {
      return null;
    }

    try {
      const platformRes = await fetch(
        `${API_URL}/invite?page=${page}&itemsPerPage=${itemsPerPage}&pagination=${pagination}`,
        {
          method: 'GET',
          headers: {
            Accept: 'application/ld+json',
            'Content-Type': 'application/ld+json',
            Authorization: `Bearer ${parsedCookies['jwt_token']}`,
          },
        },
      );

      if (platformRes.ok) {
        const text = await platformRes.text();
        const json = JSON.parse(text);

        const totalItems = json['hydra:totalItems'];
        const referralsItems = json['hydra:member'].map((el) => {
          el.createdAt = format(parseJSON(el.createdAt), 'dd.MM.yyyy');
          el.expireAt = el.expireAt
            ? (el.expireAt = format(parseJSON(el.expireAt), 'dd.MM.yyyy'))
            : '';

          return { ...el };
        });

        return {
          totalItems,
          referralsItems,
        };
      }
    } catch (error) {
      throw new GraphQLError(`GraphQLError ${error}`);
    }
  },
};
