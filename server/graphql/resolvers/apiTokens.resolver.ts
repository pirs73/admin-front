import { GraphQLError } from 'graphql';
import fetch from 'isomorphic-unfetch';
import { parseCookies } from 'nookies';
import { parseJSON, format } from 'date-fns';
import { API_URL } from '../../../constants';
import {
  ApiTokensResponse,
  GetApiTokensInputParams,
} from '../../../interfaces';

export const apiTokensQuery = {
  apiTokens: async (
    _root: undefined,
    { page, itemsPerPage, pagination }: GetApiTokensInputParams,
    context,
    _info,
  ): Promise<ApiTokensResponse | null | undefined> => {
    const req = context.req;
    const parsedCookies = parseCookies({ req });

    if (!parsedCookies['jwt_token']) {
      return null;
    }

    try {
      const platformRes = await fetch(
        `${API_URL}/api_tokens?page=${page}&itemsPerPage=${itemsPerPage}&pagination=${pagination}`,
        {
          method: 'GET',
          headers: {
            Accept: 'application/ld+json',
            'Content-Type': 'application/ld+json',
            Authorization: `Bearer ${parsedCookies['jwt_token']}`,
          },
        },
      );

      if (platformRes.ok) {
        const text = await platformRes.text();
        const json = JSON.parse(text);

        const totalItems = json['hydra:totalItems'];
        const apiTokensItems = json['hydra:member'].map((el) => {
          el.id = el['@id'] ? el['@id'].split('/').reverse()[0] : '';
          el.createdAt = format(parseJSON(el.createdAt), 'dd.MM.yyyy');
          el.lastUsedAt = el.lastUsedAt
            ? format(parseJSON(el.lastUsedAt), 'dd.MM.yyyy')
            : '';

          return { ...el };
        });

        return {
          totalItems,
          apiTokensItems,
        };
      }
    } catch (error) {
      throw new GraphQLError(`GraphQLError ${error}`);
    }
  },
};
