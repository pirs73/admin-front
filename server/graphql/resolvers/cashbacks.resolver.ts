/* eslint-disable @typescript-eslint/no-unused-vars */
import { GraphQLError } from 'graphql';
import fetch from 'isomorphic-unfetch';
import { parseCookies } from 'nookies';
import { API_URL } from '../../../constants';
import {
  GetCashbacksInputParams,
  CashbacksResponse,
  MerchantsItem,
} from '../../../interfaces';

export const cashbacksQuery = {
  cashbacks: async (
    _root: undefined,
    { page, itemsPerPage, pagination }: GetCashbacksInputParams,
    context,
    _info,
  ): Promise<CashbacksResponse | null | undefined> => {
    const req = context.req;
    const parsedCookies = parseCookies({ req });

    if (!parsedCookies['jwt_token']) {
      return null;
    }

    try {
      const platformRes = await fetch(
        `${API_URL}/cashbacks?page=${page}&itemsPerPage=${itemsPerPage}&pagination=${pagination}`,
        {
          method: 'GET',
          headers: {
            Accept: 'application/ld+json',
            'Content-Type': 'application/ld+json',
            Authorization: `Bearer ${parsedCookies['jwt_token']}`,
          },
        },
      );

      if (platformRes.ok) {
        const text = await platformRes.text();
        const json = JSON.parse(text);

        const totalItems = json['hydra:totalItems'];
        const cashbacksItems = json['hydra:member'].map((el) => {
          el.cashbackToUser = el.cashbackToUser.split('/').reverse()[0];
          el.cashbackFromMerchant = el.cashbackFromMerchant
            ? fetch(
                `${API_URL}/merchants/${
                  el.cashbackFromMerchant.split('/').reverse()[0]
                }`,
                {
                  method: 'GET',
                  headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${parsedCookies['jwt_token']}`,
                  },
                },
              )
                .then((res) => res.json())
                .then((data: any) => data.name)
            : '';
          el.cashbackFromUser = el.cashbackFromUser
            ? el.cashbackFromUser.split('/').reverse()[0]
            : '';

          return { ...el };
        });
        // console.log(cashbacksItems);
        return {
          totalItems,
          cashbacksItems,
        };
      }
    } catch (error) {
      throw new GraphQLError(`GraphQLError ${error}`);
    }
  },
};
