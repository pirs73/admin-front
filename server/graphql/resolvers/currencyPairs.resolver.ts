import { GraphQLError } from 'graphql';
import fetch from 'isomorphic-unfetch';
import { parseCookies } from 'nookies';
import { API_URL } from '../../../constants';
import {
  CurrencyPairsResponse,
  GetCurrencyPairsInputParams,
} from '../../../interfaces';

export const currencyPairsQuery = {
  currencyPairs: async (
    _root: undefined,
    { page, itemsPerPage, pagination }: GetCurrencyPairsInputParams,
    context,
    _info,
  ): Promise<CurrencyPairsResponse | null | undefined> => {
    const req = context.req;
    const parsedCookies = parseCookies({ req });

    if (!parsedCookies['jwt_token']) {
      return null;
    }

    try {
      const platformRes = await fetch(
        `${API_URL}/currency_pairs?page=${page}&itemsPerPage=${itemsPerPage}&pagination=${pagination}`,
        {
          method: 'GET',
          headers: {
            Accept: 'application/ld+json',
            'Content-Type': 'application/ld+json',
            Authorization: `Bearer ${parsedCookies['jwt_token']}`,
          },
        },
      );

      if (platformRes.ok) {
        const text = await platformRes.text();
        const json = JSON.parse(text);

        const totalItems = json['hydra:totalItems'];
        const currencyPairsItems = json['hydra:member'].map((el) => {
          el.currencyFrom = el.currencyFrom.split('/').reverse()[0];
          el.currencyTo = el.currencyTo.split('/').reverse()[0];

          return { ...el };
        });

        return {
          totalItems,
          currencyPairsItems,
        };
      }
    } catch (error) {
      throw new GraphQLError(`GraphQLError ${error}`);
    }
  },
};
