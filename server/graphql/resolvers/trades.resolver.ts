import { GraphQLError } from 'graphql';
import fetch from 'isomorphic-unfetch';
import { parseCookies } from 'nookies';
import { parseJSON, format } from 'date-fns';
import { API_URL } from '../../../constants';
import { GetTradesParams, TradesResponse } from '../../../interfaces';

export const tradesQuery = {
  trades: async (
    _root: undefined,
    {
      page,
      itemsPerPage,
      pagination,
      order,
      isCanceled,
      isPaymentSend,
      isOnDispute,
      isConfirmed,
    }: GetTradesParams,
    context,
    _info,
  ): Promise<TradesResponse | null | undefined> => {
    const req = context.req;
    const parsedCookies = parseCookies({ req });

    if (!parsedCookies['jwt_token']) {
      return null;
    }

    try {
      const platformRes = await fetch(
        `${API_URL}/trades?page=${page}&itemsPerPage=${itemsPerPage}&pagination=${pagination}&order[createdAt]=${order}&isCanceled=${isCanceled}&isPaymentSend=${isPaymentSend}&isOnDispute=${isOnDispute}&isConfirmed=${isConfirmed}`,
        {
          method: 'GET',
          headers: {
            Accept: 'application/ld+json',
            'Content-Type': 'application/ld+json',
            Authorization: `Bearer ${parsedCookies['jwt_token']}`,
          },
        },
      );

      if (platformRes.ok) {
        const text = await platformRes.text();
        const json = JSON.parse(text);

        const totalItems = json['hydra:totalItems'];
        const tradesItems = json['hydra:member'].map((el) => {
          el.currencyPair = el.currencyPair.split('/').reverse()[0];
          // el.createdAt = format(parseJSON(el.createdAt), 'dd.MM.yyyy');
          el.completedAt = el.completedAt
            ? format(parseJSON(el.completedAt), 'dd.MM.yyyy')
            : '';
          return { ...el };
        });

        return {
          totalItems,
          tradesItems,
        };
      }
    } catch (error) {
      throw new GraphQLError(`GraphQLError ${error}`);
    }
  },
};
