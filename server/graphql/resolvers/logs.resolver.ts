/* eslint-disable @typescript-eslint/no-unused-vars */
import { GraphQLError } from 'graphql';
import fetch from 'isomorphic-unfetch';
import { parseCookies } from 'nookies';
import { parseJSON, format } from 'date-fns';
import { API_URL } from '../../../constants';
import { GetLogsParams, LogsResponse } from '../../../interfaces';

export const logsQuery = {
  logs: async (
    _root: undefined,
    {
      page = 1,
      itemsPerPage = 20,
      pagination,
      createdAtBefore,
      createdAtStrictlyBefore,
      createdAtAfter,
      createdAtStrictlyAfter,
      channel = 'app',
      channels,
      level = 250,
      levels,
      belongToUser,
      belongToUsers,
    }: GetLogsParams,
    context,
    _info,
  ): Promise<LogsResponse | null | undefined> => {
    const req = context.req;
    const parsedCookies = parseCookies({ req });

    if (!parsedCookies['jwt_token']) {
      return null;
    }

    try {
      const platformRes = await fetch(
        // `${API_URL}/logs?page=${page}&itemsPerPage=${itemsPerPage}&pagination=${pagination}&createdAt[before]=${createdAtBefore}&createdAt[strictly_before]=${createdAtStrictlyBefore}&createdAt[after]=${createdAtAfter}&createdAt[strictly_after]=${createdAtStrictlyAfter}&channel=${channel}&channel[]=${channels}&level=${level}&level[]=${levels}&belongToUser=${belongToUser}&belongToUser[]=${belongToUsers}`,
        `${API_URL}/logs?page=${page}&itemsPerPage=${itemsPerPage}&pagination=${pagination}&createdAt[before]=${createdAtBefore}&createdAt[strictly_before]=${createdAtStrictlyBefore}&createdAt[after]=${createdAtAfter}&createdAt[strictly_after]=${createdAtStrictlyAfter}&channel=${channel}&level=${level}&belongToUser=${belongToUser}&belongToUser[]=${belongToUsers}`,
        {
          method: 'GET',
          headers: {
            Accept: 'application/ld+json',
            'Content-Type': 'application/ld+json',
            Authorization: `Bearer ${parsedCookies['jwt_token']}`,
          },
        },
      );

      if (platformRes.ok) {
        const text = await platformRes.text();
        const json = JSON.parse(text);
        const totalItems = json['hydra:totalItems'];
        const logsItems = json['hydra:member'].map((el) => {
          el.createdAt = format(parseJSON(el.createdAt), 'dd.MM.yyyy');

          return { ...el };
        });

        return {
          totalItems,
          logsItems,
        };
      }
    } catch (error) {
      throw new GraphQLError(`GraphQLError ${error}`);
    }
  },
};
