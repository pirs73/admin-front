import { GraphQLError } from 'graphql';
import fetch from 'isomorphic-unfetch';
import { parseCookies } from 'nookies';
import { API_URL } from '../../../constants';
import {
  GetTransactionsParams,
  TransactionsResponse,
} from '../../../interfaces';

export const transactionsQuery = {
  transactions: async (
    _root: undefined,
    { page, itemsPerPage, pagination }: GetTransactionsParams,
    context,
    _info,
  ): Promise<TransactionsResponse | null | undefined> => {
    const req = context.req;
    const parsedCookies = parseCookies({ req });

    if (!parsedCookies['jwt_token']) {
      return null;
    }

    try {
      const platformRes = await fetch(
        `${API_URL}/transactions?page=${page}&itemsPerPage=${itemsPerPage}&pagination=${pagination}`,
        {
          method: 'GET',
          headers: {
            Accept: 'application/ld+json',
            'Content-Type': 'application/ld+json',
            Authorization: `Bearer ${parsedCookies['jwt_token']}`,
          },
        },
      );

      if (platformRes.ok) {
        const text = await platformRes.text();
        const json = JSON.parse(text);

        const totalItems = json['hydra:totalItems'];
        const transactionsItems = json['hydra:member'].map((el) => {
          el.currency = el.currency.split('/').reverse()[0];
          el.trade = el.trade ? el.trade.split('/').reverse()[0] : '';

          return { ...el };
        });

        return {
          totalItems,
          transactionsItems,
        };
      }
    } catch (error) {
      throw new GraphQLError(`GraphQLError ${error}`);
    }
  },
};
