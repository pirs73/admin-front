import { adBanksQuery } from './adBanks.resoler';
import { adDetailsQuery } from './adDetails.resolver';
import { adsQuery } from './ads.resolver';
import { apiTokensQuery } from './apiTokens.resolver';
import { authMutations } from './auth.resolver';
import { balancesQuery } from './balances.resolver';
import { cashbacksQuery } from './cashbacks.resolver';
import { currenciesQuery } from './currencies.resolver';
import { currencyPairsQuery } from './currencyPairs.resolver';
import { logsQuery } from './logs.resolver';
import { merchantsQuery } from './merchants.resolver';
import { referralsQuery } from './referrals.resolver';
import { tradesQuery } from './trades.resolver';
import { transactionsQuery } from './transactions.resolver';
import { usersQueries } from './users.resolver';
import { walletsQuery } from './wallets.resolver';

export const resolvers = {
  Query: {
    ...adBanksQuery,
    ...adDetailsQuery,
    ...adsQuery,
    ...apiTokensQuery,
    ...balancesQuery,
    ...cashbacksQuery,
    ...currenciesQuery,
    ...currencyPairsQuery,
    ...logsQuery,
    ...merchantsQuery,
    ...referralsQuery,
    ...tradesQuery,
    ...transactionsQuery,
    ...usersQueries,
    ...walletsQuery,
  },
  Mutation: {
    ...authMutations,
  },
};
