import { GraphQLError } from 'graphql';
import fetch from 'isomorphic-unfetch';
import { parseCookies } from 'nookies';
import { API_URL } from '../../../constants';
import {
  GetAdsInputParams,
  AdsResponse,
  AdResourceParams,
  AdsListItem,
} from '../../../interfaces';

export const adsQuery = {
  adsList: async (
    _root: undefined,
    {
      page,
      itemsPerPage,
      pagination,
      isActive,
      currencyPair,
      currencyPairs,
      bank,
      banks,
      user,
      users,
    }: GetAdsInputParams,
    context,
    _info,
  ): Promise<AdsResponse | null | undefined> => {
    const req = context.req;
    const parsedCookies = parseCookies({ req });

    if (!parsedCookies['jwt_token']) {
      return null;
    }

    try {
      const platformRes = await fetch(
        `${API_URL}/ads?page=${page}&itemsPerPage=${itemsPerPage}&pagination=${pagination}&isActive=${isActive}&currencyPair=${currencyPair}&currencyPair[]=${currencyPairs}&bank=${bank}&bank[]=${banks}&user=${user}&user[]=${users}`,
        {
          method: 'GET',
          headers: {
            Accept: 'application/ld+json',
            'Content-Type': 'application/ld+json',
            Authorization: `Bearer ${parsedCookies['jwt_token']}`,
          },
        },
      );

      if (platformRes.ok) {
        const text = await platformRes.text();
        const json = JSON.parse(text);
        const totalItems = json['hydra:totalItems'];
        const adsItems = json['hydra:member'].map((el) => {
          el.user = el.user ? el.user.split('/').reverse()[0] : '';

          return { ...el };
        });

        return {
          totalItems,
          adsItems,
        };
      }
    } catch (error) {
      throw new GraphQLError(`GraphQLError ${error}`);
    }
  },

  getAdResource: async (
    _root: undefined,
    { uuid }: AdResourceParams,
    context,
    _info,
  ): Promise<AdsListItem | null | undefined> => {
    const req = context.req;
    const parsedCookies = parseCookies({ req });

    if (!parsedCookies['jwt_token']) {
      return null;
    }

    try {
      const platformRes = await fetch(`${API_URL}/ads/${uuid}`, {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: `Bearer ${parsedCookies['jwt_token']}`,
        },
      });

      if (platformRes.ok) {
        const data: any = await platformRes.json();
        data.user = data.user ? data.user.split('/').reverse()[0] : '';

        return data;
      }
    } catch (error) {
      throw new GraphQLError(`GraphQLError ${error}`);
    }
  },
};
