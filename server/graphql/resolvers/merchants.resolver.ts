import { GraphQLError } from 'graphql';
import fetch from 'isomorphic-unfetch';
import { parseCookies } from 'nookies';
import { parseJSON, format } from 'date-fns';
import { API_URL } from '../../../constants';
import { GetMerchantsParams, MerchantsResponse } from '../../../interfaces';

export const merchantsQuery = {
  merchants: async (
    _root: undefined,
    { page, itemsPerPage, pagination }: GetMerchantsParams,
    context,
    _info,
  ): Promise<MerchantsResponse | null | undefined> => {
    const req = context.req;
    const parsedCookies = parseCookies({ req });

    if (!parsedCookies['jwt_token']) {
      return null;
    }

    try {
      const platformRes = await fetch(
        `${API_URL}/merchants?page=${page}&itemsPerPage=${itemsPerPage}&pagination=${pagination}`,
        {
          method: 'GET',
          headers: {
            Accept: 'application/ld+json',
            'Content-Type': 'application/ld+json',
            Authorization: `Bearer ${parsedCookies['jwt_token']}`,
          },
        },
      );

      if (platformRes.ok) {
        const text = await platformRes.text();
        const json = JSON.parse(text);

        const totalItems = json['hydra:totalItems'];
        const merchantsItems = json['hydra:member'].map((el) => {
          el.user = el.user.split('/').reverse()[0];
          el.createdAt = format(parseJSON(el.createdAt), 'dd.MM.yyyy');

          return { ...el };
        });

        return {
          totalItems,
          merchantsItems,
        };
      }
    } catch (error) {
      throw new GraphQLError(`GraphQLError ${error}`);
    }
  },
};
