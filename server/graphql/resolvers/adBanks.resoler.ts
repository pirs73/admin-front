import { GraphQLError } from 'graphql';
import fetch from 'isomorphic-unfetch';
import { parseCookies } from 'nookies';
import { API_URL } from '../../../constants';
import { AdBanksResponse, GetAdBanksInputParams } from '../../../interfaces';

export const adBanksQuery = {
  adBanks: async (
    _root: undefined,
    { pagination }: GetAdBanksInputParams,
    context,
    _info,
  ): Promise<AdBanksResponse | null | undefined> => {
    const req = context.req;
    const parsedCookies = parseCookies({ req });

    if (!parsedCookies['jwt_token']) {
      return null;
    }

    try {
      const platformRes = await fetch(
        `${API_URL}/ad_banks?pagination=${pagination}`,
        {
          method: 'GET',
          headers: {
            Accept: 'application/ld+json',
            'Content-Type': 'application/ld+json',
            Authorization: `Bearer ${parsedCookies['jwt_token']}`,
          },
        },
      );

      if (platformRes.ok) {
        const text = await platformRes.text();
        const json = JSON.parse(text);
        const totalItems = json['hydra:totalItems'];
        const adBanksItems = json['hydra:member'];

        return {
          totalItems,
          adBanksItems,
        };
      }
    } catch (error) {
      throw new GraphQLError(`GraphQLError ${error}`);
    }
  },
};
