import { GraphQLError } from 'graphql';
import fetch from 'isomorphic-unfetch';
import { parseCookies } from 'nookies';
import { API_URL } from '../../../constants';
import { GetWalletsParams, WalletsResponse } from '../../../interfaces';
//git
export const walletsQuery = {
  wallets: async (
    _root: undefined,
    { page, itemsPerPage, pagination }: GetWalletsParams,
    context,
    _info,
  ): Promise<WalletsResponse | null | undefined> => {
    const req = context.req;
    const parsedCookies = parseCookies({ req });

    if (!parsedCookies['jwt_token']) {
      return null;
    }

    try {
      const platformRes = await fetch(
        // `${API_URL}/wallets?page=${page}&itemsPerPage=${itemsPerPage}&pagination=${pagination}&address=${address}&address[]=${addresses}&user=${user}&user[]=${users}`,
        `${API_URL}/wallets?page=${page}&itemsPerPage=${itemsPerPage}&pagination=${pagination}`,
        {
          method: 'GET',
          headers: {
            Accept: 'application/ld+json',
            'Content-Type': 'application/ld+json',
            Authorization: `Bearer ${parsedCookies['jwt_token']}`,
          },
        },
      );

      if (platformRes.ok) {
        const text = await platformRes.text();
        const json = JSON.parse(text);

        const totalItems = json['hydra:totalItems'];
        const walletsItems = json['hydra:member'];

        return {
          totalItems,
          walletsItems,
        };
      }
    } catch (error) {
      throw new GraphQLError(`GraphQLError ${error}`);
    }
  },
};
