/* eslint-disable @typescript-eslint/no-unused-vars */
import { GraphQLError } from 'graphql';
import fetch from 'isomorphic-unfetch';
import { parseCookies } from 'nookies';
import { parseJSON, format } from 'date-fns';
import { API_URL } from '../../../constants';
import {
  GetUsersParams,
  UsersResponse,
  ViewerResponse,
} from '../../../interfaces';

export const usersQueries = {
  viewer: async (
    _root: undefined,
    _args: any,
    context: any,
    _info: any,
  ): Promise<ViewerResponse | null | undefined> => {
    const req = context.req;
    const parsedCookies = parseCookies({ req });
    if (!parsedCookies['jwt_token']) {
      return null;
    }

    if (parsedCookies['jwt_token']) {
      try {
        const platformRes = await fetch(`${API_URL}/me`, {
          method: 'GET',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: `Bearer ${parsedCookies['jwt_token']}`,
          },
        });
        if (platformRes.ok) {
          const platformData = await platformRes.json();
          const data: any = platformData;

          return {
            viewer: data,
            token: parsedCookies['jwt_token'],
          };
        } else {
          throw new GraphQLError(
            'Authentication token is invalid, please log in',
          );
        }
      } catch (error) {
        console.error(error);
        throw new GraphQLError(
          'Authentication token is invalid, please log in',
          {
            extensions: {
              code: 'UNAUTHENTICATED',
            },
          },
        );
      }
    }
  },

  users: async (
    _root: undefined,
    {
      page,
      itemsPerPage,
      pagination,
      isOnline,
      isActivated,
      isTrader,
      isMerchant,
      createdAtBefore,
      createdAtStrictlyBefore,
      createdAtAfter,
      createdAtStrictlyAfter,
      lastOnlineAtBefore,
      lastOnlineAtStrictlyBefore,
      lastOnlineAtAfter,
      lastOnlineAtStrictlyAfter,
      username,
      usernames,
    }: GetUsersParams,
    context,
    _info,
  ): Promise<UsersResponse | null | undefined> => {
    const req = context.req;
    const parsedCookies = parseCookies({ req });

    if (!parsedCookies['jwt_token']) {
      return null;
    }

    try {
      const platformRes = await fetch(
        // `${API_URL}/users?page=${page}&itemsPerPage=${itemsPerPage}&pagination=${pagination}&isOnline=${isOnline}&isActivated=${isActivated}&isTrader${isTrader}&isMerchant=${isMerchant}&createdAt[before]=${createdAtBefore}&createdAt[strictly_before]=${createdAtStrictlyBefore}&createdAt[after]=${createdAtAfter}&createdAt[strictly_after]=${createdAtStrictlyAfter}&lastOnlineAt[before]=${lastOnlineAtBefore}&lastOnlineAt[strictly_before]=${lastOnlineAtStrictlyBefore}&lastOnlineAt[after]=${lastOnlineAtAfter}&lastOnlineAt[strictly_after]=${lastOnlineAtStrictlyAfter}&username=${username}&username[]=${usernames}`,
        `${API_URL}/users?page=${page}&itemsPerPage=${itemsPerPage}&pagination=${pagination}`,
        {
          method: 'GET',
          headers: {
            Accept: 'application/ld+json',
            'Content-Type': 'application/ld+json',
            Authorization: `Bearer ${parsedCookies['jwt_token']}`,
          },
        },
      );

      if (platformRes.ok) {
        const text = await platformRes.text();
        const json = JSON.parse(text);

        const totalItems = json['hydra:totalItems'];

        const usersItems = json['hydra:member'].map((el) => {
          el.phone = el.phone ? el.phone : '';
          el.referredBy = el.referredBy ? el.referredBy : '';
          el.allowedNumberOfReferrals = el.allowedNumberOfReferrals
            ? el.allowedNumberOfReferrals
            : 0;
          el.referral = el.referral ? el.referral : '';
          el.createdAt = format(parseJSON(el.createdAt), 'dd.MM.yyyy');
          el.lastOnlineAt = el.lastOnlineAt
            ? format(parseJSON(el.lastOnlineAt), 'dd.MM.yyyy')
            : '';

          return {
            ...el,
          };
        });

        return {
          totalItems,
          usersItems,
        };
      }
    } catch (error) {
      throw new GraphQLError(`GraphQLError ${error}`);
    }
  },
};
