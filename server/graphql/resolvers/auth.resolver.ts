import { GraphQLError } from 'graphql';
import fetch from 'isomorphic-unfetch';
import { parseCookies, setCookie, destroyCookie } from 'nookies';
import jwt_decode from 'jwt-decode';
import { API_URL } from '../../../constants';
import {
  LoginDataArgs,
  RegisterDataArgs,
  LoginDataResponse,
} from '../../../interfaces';

// export const authQuery = {};

export const authMutations = {
  signUp: async (
    _parent,
    args: { input: RegisterDataArgs },
    _context,
    _info,
  ): Promise<boolean> => {
    try {
      const platformRes = await fetch(`${API_URL}/register`, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(args.input),
      });

      if (platformRes.ok) {
        return true;
      } else {
        throw new GraphQLError('Something went wrong');
      }
    } catch (error) {
      console.error(error);
      throw new GraphQLError(`Something went wrong ${error}`);
    }
  },

  signIn: async (
    _parent,
    args: { input: LoginDataArgs },
    context,
    _info,
  ): Promise<LoginDataResponse> => {
    try {
      const platformRes = await fetch(`${API_URL}/auth`, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(args.input),
      });

      if (platformRes.ok) {
        const platformData = await platformRes.json();

        const { token }: any = platformData;
        const res = context.res;

        const cookieOut = platformRes.headers.getSetCookie()[0];

        const refresh_token = cookieOut.split(';')[0].split('=')[1];

        setCookie({ res }, 'jwt_token', token, {
          httpOnly: true,
          secure: false,
          // secure: process.env.NODE_ENV !== 'development',
          // sameSite: 'lax',
          sameSite: process.env.NODE_ENV === 'production' ? 'strict' : 'lax',
          maxAge: 60 * 10,
          path: '/',
        });

        setCookie({ res }, 'refresh_token', refresh_token, {
          httpOnly: true,
          secure: false,
          // secure: process.env.NODE_ENV !== 'development',
          // sameSite: 'lax',
          sameSite: process.env.NODE_ENV === 'production' ? 'strict' : 'lax',
          maxAge: 60 * 60 * 24,
          path: '/',
        });

        return { token };
      } else {
        throw new GraphQLError('Invalid email and password combination');
      }
    } catch (error) {
      console.error(error);
      throw new GraphQLError(`Something went wrong ${error}`);
    }
  },

  signOut: async (_parent, _args, context, _info) => {
    try {
      const res = context.res;
      destroyCookie({ res }, 'jwt_token', {
        expires: new Date(0),
        path: '/',
      });

      destroyCookie({ res }, 'refresh_token', {
        expires: new Date(0),
        path: '/',
      });

      return true;
    } catch (error) {
      console.error(error);
      throw new GraphQLError(`${error}`);
    }
  },

  refreshToken: async (
    _root: undefined,
    _args: any,
    context: any,
    _info: any,
  ): Promise<any> => {
    const req = context.req;
    const parsedCookies = parseCookies({ req });

    if (!parsedCookies['refresh_token']) {
      return null;
    }

    if (parsedCookies['refresh_token']) {
      try {
        const platformRes = await fetch(`${API_URL}/token/refresh`, {
          method: 'POST',
          credentials: 'same-origin',
          mode: 'same-origin',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Cookie: `refresh_token=${parsedCookies['refresh_token']}`,
          },
        });

        if (platformRes.ok) {
          const res = context.res;
          const platformData = await platformRes.json();

          const { token }: any = platformData;

          setCookie({ res }, 'jwt_token', token, {
            httpOnly: true,
            secure: false,
            // secure: process.env.NODE_ENV !== 'development',
            // sameSite: 'lax',
            sameSite: process.env.NODE_ENV === 'production' ? 'strict' : 'lax',
            maxAge: 60 * 10,
            path: '/',
          });

          return { jwt_token: token };
        } else {
          return null;
        }
      } catch (error) {
        throw new GraphQLError(`Something went wrong ${error}`);
      }
    }
  },

  checkAccessToken: (
    _root: undefined,
    _args: any,
    context: any,
    _info: any,
  ) => {
    const req = context.req;
    const parsedCookies = parseCookies({ req });
    const accessToken = parsedCookies['jwt_token'];

    if (!accessToken) {
      return false;
    }

    if (accessToken) {
      const { exp }: any = jwt_decode(accessToken);
      const isAccessTokenExpired = Date.now() / 1000 > exp;

      if (isAccessTokenExpired) {
        return false;
      } else {
        return true;
      }
    }
  },
};
