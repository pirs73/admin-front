# Front Admin

## Install

```bash
engines:
    node: ">=18",
    npm: ">=9"
```

```bash
npm install
```

## Run development mode

```bash
npm run dev
```

## Run production mode

```bash
npm run build
npm run start
```

## Docker

```bash
$ docker build --file=front.dockerfile -t frontadmin .
$ docker build --file=socket.dockerfile -t socketadmin .
$ docker-compose up -d
```
