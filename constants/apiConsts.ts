export const API_URL =
  process.env.NEXT_PUBLIC_API_URL || 'http://api.pay4me.online/api';
export const NEXT_URL =
  process.env.NEXT_PUBLIC_FRONTEND_URL || 'http://localhost:3001/api';
export const PORT = process.env.NEXT_PUBLIC_PORT || 3001;
