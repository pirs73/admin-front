import { removeConnectedUser } from '../serverStore';

export const disconnectHandler = (socket) => {
  removeConnectedUser(socket.id);
};
