import { addNewConnectedUser } from '../serverStore';

export const newConnectionHandler = (socket, _io) => {
  const userDetails = socket.user;

  addNewConnectedUser({
    socketId: socket.id,
    userName: userDetails.username,
  });
};
