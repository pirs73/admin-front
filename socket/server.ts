import express from 'express';
import http from 'http';
import cors from 'cors';

import registerSocketServer from './socketServer';

const PORT = parseInt(process.env.API_PORT || '5002', 10);

const app = express();
app.use(express.json());

const allowedOrigins = [
  `http://localhost`,
  `http://localhost:3001`,
  `http://front.pay4me.online`,
  `http://front.pay4me.online:3001`,
];

app.use(
  cors({
    origin: function (origin, callback) {
      // allow requests with no origin
      // (like mobile apps or curl requests)
      if (!origin) return callback(null, true);
      if (allowedOrigins.indexOf(origin) === -1) {
        const msg =
          'The CORS policy for this site does not ' +
          'allow access from the specified Origin.';
        return callback(new Error(msg), false);
      }
      return callback(null, true);
    },
    credentials: true,
  }),
);

const server = http.createServer(app);
registerSocketServer(server);

server.listen(PORT, () => {
  console.info(`Server is listening on ${PORT}`);
});
