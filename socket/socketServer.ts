import * as socketio from 'socket.io';
import verifyTokenSocket from './middleware/authSocket';
import { newConnectionHandler } from './socketHandlers/newConnectionHandler';
import { disconnectHandler } from './socketHandlers/disconnectHandler';
import { setSocketServerInstance } from './serverStore';

const registerSocketServer = (server) => {
  const io: socketio.Server = new socketio.Server();

  io.attach(server, {
    cors: {
      origin: [
        `http://localhost`,
        `http://localhost:3001`,
        `http://front.pay4me.online`,
        `http://front.pay4me.online:3001`,
      ],
      methods: ['GET', 'POST', 'OPTIONS'],
    },
  });

  setSocketServerInstance(io);

  io.use((socket, next) => {
    verifyTokenSocket(socket, next);
  });

  io.on('connection', (socket) => {
    console.info('user connected');
    console.info(socket.id);

    newConnectionHandler(socket, io);

    socket.on('disconnect', () => {
      disconnectHandler(socket);
    });
  });
};

export default registerSocketServer;
