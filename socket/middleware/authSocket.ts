import jwt_decode from 'jwt-decode';

const verifyTokenSocket = (socket, next) => {
  const token = socket.handshake.auth?.token;
  try {
    const decoded = jwt_decode(token);
    socket.user = decoded;
  } catch (error) {
    console.error(error);
    const socketError = new Error('NOT_AUTHORIZED');

    return next(socketError);
  }

  next();
};

export default verifyTokenSocket;
