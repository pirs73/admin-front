import * as socketio from 'socket.io';
const connectedUsers = new Map<string, Record<string, string>>();

let io: socketio.Server | null = null;

export const setSocketServerInstance = (ioInstance) => {
  io = ioInstance;
};

export const getSocketServerInstance = () => {
  if (io) {
    return io;
  }
};

export const addNewConnectedUser = ({ socketId, userName }) => {
  connectedUsers.set(socketId, { userName });
  console.info('new connected users');
  console.info(connectedUsers);
};

export const removeConnectedUser = (socketId) => {
  if (connectedUsers.has(socketId)) {
    connectedUsers.delete(socketId);
    console.info('new disconnected users');
    console.info(connectedUsers);
  }
};

export const getActiveConnections = (userName) => {
  const activeConnections: string[] = [];

  connectedUsers.forEach(function (value, key) {
    if (value.userName === userName) {
      activeConnections.push(key);
    }
  });

  return activeConnections;
};
