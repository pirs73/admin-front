import { getActiveConnections, getSocketServerInstance } from '../serverStore';

// вызвать эту функцию в ресолвере при создании трейда мерчантом
export const updateTraders = async (userName) => {
  try {
    console.info(userName);
    /** Написать сначало запрос на поиск конктретного юзера, а возможно и всех тредеров */

    const pendingConfirmed = [userName];

    /** find all active connections of specific userName */
    const receiverList = getActiveConnections(userName);

    const io = getSocketServerInstance();
    console.info(io);

    receiverList.forEach((receiverSocketId) => {
      io?.to(receiverSocketId).emit('new-trade', {
        pendingConfirmed: pendingConfirmed ? pendingConfirmed : [],
      });
    });
  } catch (error) {
    console.error(error);
  }
};
