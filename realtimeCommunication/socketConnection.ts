import io, { Socket } from 'socket.io-client';
import { newTradeCreatedVar } from '@app/cache';
import { DefaultEventsMap } from 'socket.io/dist/typed-events';

let socket: Socket<DefaultEventsMap, DefaultEventsMap> | null = null;

export const connectWithSocketServer = (token) => {
  socket = io('http://localhost:5002', {
    auth: {
      token,
    },
  });

  socket?.on('connect', () => {
    console.info('successfully connected with socket.io server');
    console.info(socket?.id);
  });

  socket?.on('new-trade', (data) => {
    const { newTradeCreated } = data;

    console.info('new trade event came');
    console.info(newTradeCreated);

    newTradeCreatedVar(newTradeCreated);
  });
};
