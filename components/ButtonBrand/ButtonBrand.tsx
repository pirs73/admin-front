import React from 'react';
import cn from 'classnames';
import { ButtonBrandProps } from './ButtonBrand.props';
import styles from './ButtonBrand.module.css';

const ButtonBrand = ({ className, children, ...props }: ButtonBrandProps) => {
  return (
    <button className={cn(styles.button, className)} {...props}>
      {children}
    </button>
  );
};

export { ButtonBrand };
