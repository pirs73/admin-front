import cn from 'classnames';
import { LabelProps } from './Label.props';
import styles from './Label.module.css';

const Label = ({
  children,
  className,
  htmlFor,
  required,
  ...props
}: LabelProps): JSX.Element => {
  return (
    <label htmlFor={htmlFor} className={cn(styles.label, className)} {...props}>
      {children}
      {required && <span className={styles.required}>*</span>}
    </label>
  );
};

export { Label };
