import React from 'react';
import cn from 'classnames';
import { ButtonProps } from './Button.props';
import styles from './Button.module.css';

const Button = ({ className, children, ...props }: ButtonProps) => {
  return (
    <button className={cn(styles.button, className)} {...props}>
      {children}
    </button>
  );
};

export { Button };
