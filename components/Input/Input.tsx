import { ForwardedRef, forwardRef } from 'react';
import cn from 'classnames';
import { Label } from '@app/components';
import { InputProps } from './Input.props';
import styles from './Input.module.css';

const Input = forwardRef(
  (
    { type, label, name, required, className, ...props }: InputProps,
    ref: ForwardedRef<HTMLInputElement>,
  ): JSX.Element => {
    return (
      <div className={cn(styles.inputWrap, className)}>
        {label && (
          <Label htmlFor={name} required={required}>
            {label}
          </Label>
        )}
        <input
          type={type}
          id={name}
          className={cn(styles.input)}
          ref={ref}
          {...props}
        />
      </div>
    );
  },
);

export { Input };
