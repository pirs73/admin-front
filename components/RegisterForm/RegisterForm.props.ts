import { DetailedHTMLProps, HTMLAttributes } from 'react';
import { SubmitHandler } from 'react-hook-form';
import { RegisterDataArgs } from '@app/interfaces';

export interface RegisterFormProps
  extends DetailedHTMLProps<HTMLAttributes<HTMLFormElement>, HTMLFormElement> {
  handleRegisterSubmit: SubmitHandler<RegisterDataArgs>;
  loading?: boolean;
}
