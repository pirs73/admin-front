import Link from 'next/link';
import { Controller, useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { RegisterDataArgs } from '@app/interfaces';
import { Button, Input, InputPassword } from '@app/components';
import { RegisterFormProps } from './RegisterForm.props';
import styles from './RegisterForm.module.css';

const schema = yup.object().shape({
  username: yup.string().min(3).required('Username is required'),
  password: yup.string().min(6).required('Password is required'),
  referralCode: yup.string().min(3).required('referralCode is required'),
});

const RegisterForm = ({ loading, handleRegisterSubmit }: RegisterFormProps) => {
  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm<RegisterDataArgs>({
    mode: 'onBlur',
    resolver: yupResolver(schema),
  });

  return (
    <form onSubmit={handleSubmit(handleRegisterSubmit)}>
      <div className="mb-3">
        <Controller
          name="username"
          control={control}
          rules={{ required: true }}
          render={({ field }) => (
            <Input
              type="text"
              label="Username"
              required
              placeholder="Username *"
              {...field}
            />
          )}
          defaultValue={''}
        />
        <div className={styles.warningWrapper}>
          {errors && (
            <span className={styles.warning}>
              {(errors.username && errors.username.message)?.toString()}
            </span>
          )}
        </div>
      </div>
      <div className="mb-3">
        <Controller
          name="password"
          control={control}
          rules={{ required: true }}
          render={({ field }) => (
            <InputPassword
              label="Password"
              placeholder="Password *"
              required
              {...field}
            />
          )}
          defaultValue={''}
        />
        <div className={styles.warningWrapper}>
          {errors && (
            <span className={styles.warning}>
              {(errors.password && errors.password.message)?.toString()}
            </span>
          )}
        </div>
      </div>
      <div className="mb-3">
        <Controller
          name="referralCode"
          control={control}
          rules={{ required: true }}
          render={({ field }) => (
            <Input
              type="text"
              label="referralCode"
              placeholder="referralCode *"
              required
              {...field}
            />
          )}
          defaultValue={''}
        />
        <div className={styles.warningWrapper}>
          {errors && (
            <span className={styles.warning}>
              {(errors.referralCode && errors.referralCode.message)?.toString()}
            </span>
          )}
        </div>
      </div>
      {loading && <span>Singin in...</span>}
      {!loading && (
        <div className={styles.buttonWrapper}>
          <Button type="submit" className={styles.button}>
            Submit
          </Button>
          <span className={styles.text}>
            &nbsp; or <Link href="/login">Login</Link>
          </span>
        </div>
      )}
    </form>
  );
};

export { RegisterForm };
