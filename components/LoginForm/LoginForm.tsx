import Link from 'next/link';
import { Controller, useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { LoginDataArgs } from '@app/interfaces';
import { Button, Input, InputPassword } from '@app/components';
import { LoginFormProps } from './LoginForm.props';
import styles from './LoginForm.module.css';

const schema = yup.object().shape({
  username: yup.string().min(3).required('Username is required'),
  password: yup.string().min(6).required('Password is required'),
});

const LoginForm = ({ loading, handleLoginSubmit }: LoginFormProps) => {
  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm<LoginDataArgs>({
    mode: 'onBlur',
    resolver: yupResolver(schema),
  });

  return (
    <form onSubmit={handleSubmit(handleLoginSubmit)}>
      <div className="mb-3">
        <Controller
          name="username"
          control={control}
          rules={{ required: true }}
          render={({ field }) => (
            <Input
              type="text"
              label="Username"
              required
              placeholder="Username *"
              {...field}
            />
          )}
          defaultValue={''}
        />
        <div className={styles.warningWrapper}>
          {errors && (
            <span className={styles.warning}>
              {(errors.username && errors.username.message)?.toString()}
            </span>
          )}
        </div>
      </div>
      <div className="mb-3">
        <Controller
          name="password"
          control={control}
          rules={{ required: true }}
          render={({ field }) => (
            <InputPassword
              label="Password"
              placeholder="Password *"
              required
              {...field}
            />
          )}
          defaultValue={''}
        />
        <div className={styles.warningWrapper}>
          {errors && (
            <span className={styles.warning}>
              {(errors.password && errors.password.message)?.toString()}
            </span>
          )}
        </div>
      </div>
      {loading && <span>Singin in...</span>}
      {!loading && (
        <div className={styles.buttonWrapper}>
          <Button type="submit" className={styles.button}>
            Submit
          </Button>
          <span className={styles.text}>
            &nbsp; or <Link href="/register">Register</Link>
          </span>
        </div>
      )}
    </form>
  );
};

export { LoginForm };
