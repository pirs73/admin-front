import { DetailedHTMLProps, HTMLAttributes } from 'react';
import { SubmitHandler } from 'react-hook-form';
import { LoginDataArgs } from '@app/interfaces';

export interface LoginFormProps
  extends DetailedHTMLProps<HTMLAttributes<HTMLFormElement>, HTMLFormElement> {
  handleLoginSubmit: SubmitHandler<LoginDataArgs>;
  loading?: boolean;
}
