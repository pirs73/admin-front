import { DetailedHTMLProps, InputHTMLAttributes } from 'react';
import { FieldErrors } from 'react-hook-form';
export interface InputPasswordProps
  extends DetailedHTMLProps<
    InputHTMLAttributes<HTMLInputElement>,
    HTMLInputElement
  > {
  label?: string;
  error?: FieldErrors;
  onCangePassword?: () => void;
}
