import { ForwardedRef, forwardRef, useState } from 'react';
import cn from 'classnames';
import { Label } from '@app/components';
import HideIcon from './icons/hide.svg';
import ShowIcon from './icons/show.svg';
import { InputPasswordProps } from './InputPassword.props';
import styles from '@app/components/Input/Input.module.css';

const InputPassword = forwardRef(
  (
    {
      label,
      name,
      required,
      onCangePassword,
      className,
      error,
      ...props
    }: InputPasswordProps,
    ref: ForwardedRef<HTMLInputElement>,
  ): JSX.Element => {
    const [showPassword, setShowPassword] = useState<boolean>(false);

    return (
      <div className={cn(styles.inputWrap, className)}>
        {label && (
          <Label htmlFor={name} required={required}>
            {label}
          </Label>
        )}
        <input
          type={showPassword ? 'text' : 'password'}
          id={name}
          name={name}
          className={cn(styles.input, {
            [styles.error]: error,
          })}
          ref={ref}
          onChange={onCangePassword}
          {...props}
        />
        <div
          className={`${styles.eye}`}
          onClick={() => setShowPassword(!showPassword)}
        >
          {showPassword ? (
            <HideIcon className={styles.icon} />
          ) : (
            <ShowIcon className={styles.icon} />
          )}
        </div>
      </div>
    );
  },
);

export { InputPassword };
