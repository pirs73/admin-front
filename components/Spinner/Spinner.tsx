import CircularProgress from '@mui/material/CircularProgress';
import Box from '@mui/material/Box';
import styles from './Spinner.module.css';

const Spinner = () => {
  return (
    <Box className={styles.spinner_container}>
      <CircularProgress />
      <p className={styles.spinner_color}>Load data...</p>
    </Box>
  );
};

export { Spinner };
