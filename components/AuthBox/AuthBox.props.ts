import { DetailedHTMLProps, HTMLAttributes, ReactNode } from 'react';

export interface AuthBoxProps
  extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
  children: ReactNode;
  isRegisterForm?: boolean;
}
