import { Box } from '@mui/material';
import cn from 'classnames';
import { AuthBoxProps } from './AuthBox.props';
import styles from './AuthBox.module.css';

export const AuthBox = ({
  isRegisterForm,
  children,
  ...props
}: AuthBoxProps) => {
  return (
    <div className={cn(styles.auth_box_wrapper)} {...props}>
      <Box
        className={cn({
          [styles.auth_box]: !isRegisterForm,
          [styles.auth_box_register]: isRegisterForm,
        })}
      >
        {children}
      </Box>
    </div>
  );
};
