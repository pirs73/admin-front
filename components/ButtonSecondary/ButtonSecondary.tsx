import React from 'react';
import cn from 'classnames';
import { ButtonSecondaryProps } from './ButtonSecondary.props';
import styles from './ButtonSecondary.module.css';

const ButtonSecondary = ({
  className,
  children,
  ...props
}: ButtonSecondaryProps) => {
  return (
    <button className={cn(styles.button, className)} {...props}>
      {children}
    </button>
  );
};

export { ButtonSecondary };
