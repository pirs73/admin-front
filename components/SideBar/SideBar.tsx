import Link from 'next/link';
import cn from 'classnames';
import { SideBarMenu } from '@app/components';
import { SideBarProps } from './SideBar.props';
import styles from './SideBar.module.css';

const SideBar = ({ className, ...props }: SideBarProps) => {
  return (
    <div className={cn(styles.sidebar, className)} {...props}>
      <div>
        <h6>Online</h6>
      </div>
      <div>
        <SideBarMenu />
      </div>
      <div>
        <h6>User</h6>
        <Link href="/logout">LogOut</Link>
      </div>
    </div>
  );
};

export { SideBar };
