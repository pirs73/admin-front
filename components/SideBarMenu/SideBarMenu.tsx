import * as React from 'react';
import Link from 'next/link';
import Box from '@mui/material/Box';
import List from '@mui/material/List';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import FormatListBulletedIcon from '@mui/icons-material/FormatListBulleted';

import styles from './SideBarMenu.module.css';

const SideBarMenu = () => {
  return (
    <Box sx={{ width: '100%', maxWidth: 360, bgcolor: 'background.paper' }}>
      <List component="nav" aria-label="main mailbox folders">
        <div className={styles.linkBox}>
          <ListItemIcon>
            <FormatListBulletedIcon className={styles.icon} />
          </ListItemIcon>
          <Link href="/ads/" className={styles.link}>
            <ListItemText primary="Ads" />
          </Link>
        </div>
        <div className={styles.linkBox}>
          <ListItemIcon>
            <FormatListBulletedIcon className={styles.icon} />
          </ListItemIcon>
          <Link href="/ad_details/" className={styles.link}>
            <ListItemText primary="Ad Details" />
          </Link>
        </div>
        <div className={styles.linkBox}>
          <ListItemIcon>
            <FormatListBulletedIcon className={styles.icon} />
          </ListItemIcon>
          <Link href="/ad_banks/" className={styles.link}>
            <ListItemText primary="Ad Banks" />
          </Link>
        </div>
        <div className={styles.linkBox}>
          <ListItemIcon>
            <FormatListBulletedIcon className={styles.icon} />
          </ListItemIcon>
          <Link href="/api_tokens/" className={styles.link}>
            <ListItemText primary="Api Tokens" />
          </Link>
        </div>
        <div className={styles.linkBox}>
          <ListItemIcon>
            <FormatListBulletedIcon className={styles.icon} />
          </ListItemIcon>
          <Link href="/balances/" className={styles.link}>
            <ListItemText primary="Balances" />
          </Link>
        </div>
        <div className={styles.linkBox}>
          <ListItemIcon>
            <FormatListBulletedIcon className={styles.icon} />
          </ListItemIcon>
          <Link href="/cashbacks/" className={styles.link}>
            <ListItemText primary="Cashbacks" />
          </Link>
        </div>
        <div className={styles.linkBox}>
          <ListItemIcon>
            <FormatListBulletedIcon className={styles.icon} />
          </ListItemIcon>
          <Link href="/currencies/" className={styles.link}>
            <ListItemText primary="Currencies" />
          </Link>
        </div>
        <div className={styles.linkBox}>
          <ListItemIcon>
            <FormatListBulletedIcon className={styles.icon} />
          </ListItemIcon>
          <Link href="/currency_pairs/" className={styles.link}>
            <ListItemText primary="Currency Pairs" />
          </Link>
        </div>
        <div className={styles.linkBox}>
          <ListItemIcon>
            <FormatListBulletedIcon className={styles.icon} />
          </ListItemIcon>
          <Link href="/merchants/" className={styles.link}>
            <ListItemText primary="Merchants" />
          </Link>
        </div>
        <div className={styles.linkBox}>
          <ListItemIcon>
            <FormatListBulletedIcon className={styles.icon} />
          </ListItemIcon>
          <Link href="/referrals/" className={styles.link}>
            <ListItemText primary="Referrals" />
          </Link>
        </div>
        <div className={styles.linkBox}>
          <ListItemIcon>
            <FormatListBulletedIcon className={styles.icon} />
          </ListItemIcon>
          <Link href="/trades/" className={styles.link}>
            <ListItemText primary="Trades" />
          </Link>
        </div>
        <div className={styles.linkBox}>
          <ListItemIcon>
            <FormatListBulletedIcon className={styles.icon} />
          </ListItemIcon>
          <Link href="/transactions/" className={styles.link}>
            <ListItemText primary="Transactions" />
          </Link>
        </div>
        <div className={styles.linkBox}>
          <ListItemIcon>
            <FormatListBulletedIcon className={styles.icon} />
          </ListItemIcon>
          <Link href="/users/" className={styles.link}>
            <ListItemText primary="Users" />
          </Link>
        </div>
        <div className={styles.linkBox}>
          <ListItemIcon>
            <FormatListBulletedIcon className={styles.icon} />
          </ListItemIcon>
          <Link href="/wallets/" className={styles.link}>
            <ListItemText primary="Wallets" />
          </Link>
        </div>
        <div className={styles.linkBox}>
          <ListItemIcon>
            <FormatListBulletedIcon className={styles.icon} />
          </ListItemIcon>
          <Link href="/logs/" className={styles.link}>
            <ListItemText primary="Logs" />
          </Link>
        </div>
      </List>
    </Box>
  );
};

export { SideBarMenu };
