export interface GetTradesParams {
  page?: number;
  itemsPerPage?: number;
  pagination?: boolean;
  order?: 'asc' | 'desc';
  isCanceled?: boolean;
  isPaymentSend?: boolean;
  isOnDispute?: boolean;
  isConfirmed?: boolean;
}

export interface TradesItem {
  uuid?: string | null;
  currencyPair: string;
  amount: string;
  rate: string;
  createdAt: string;
  completedAt?: string | null;
  user?: string | null;
  adDetail?: string | null;
  isConfirmed: boolean;
  isOnDispute: boolean;
  isPaymentSend: boolean;
  isCanceled: boolean;
}

export interface TradesResponse {
  totalItems: number;
  tradesItems: TradesItem[];
}
