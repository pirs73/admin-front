export interface LoginDataArgs {
  username: string;
  password: string;
}

export interface LoginDataResponse {
  token: string;
}

export interface RegisterDataArgs {
  username: string;
  password: string;
  referralCode: string;
}

export interface RegisterDataResponse {
  context: string;
  id: string;
  type: string;
}

export interface Viewer {
  username: string;
  scopes: string[];
}

export interface ViewerResponse {
  viewer: Viewer;
  token: string;
}
