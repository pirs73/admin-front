export interface GetCurrencyPairsInputParams {
  page?: number;
  itemsPerPage?: number;
  pagination?: boolean;
}

export interface CurrencyPairsItem {
  name: string;
  currencyFrom: string;
  currencyTo: string;
  exchangeRate?: string;
}

export interface CurrencyPairsResponse {
  totalItems: number;
  currencyPairsItems: CurrencyPairsItem[];
}
