export interface GetAdsInputParams {
  page?: number;
  itemsPerPage?: number;
  pagination?: boolean;
  isActive?: boolean;
  currencyPair?: string;
  currencyPairs?: string[];
  bank?: string;
  banks?: string[];
  user?: string;
  users?: string[];
}

export interface AdsDetail {
  uuid?: string | null;
  number: string;
  text?: string | null;
  transactionSum: string;
}

export interface AdsListItem {
  uuid?: string | null;
  currencyPair: {
    name: string;
  };
  minAmount: string;
  maxAmount: string;
  user?: string | null;
  details: AdsDetail[];
  isActive: boolean;
  bank: {
    name: string;
    shortName: string;
  };
}

export interface AdsResponse {
  totalItems: number;
  adsItems: AdsListItem[];
}

export interface AdResourceParams {
  uuid: string;
}
