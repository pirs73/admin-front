export interface GetTransactionsParams {
  page?: number;
  itemsPerPage?: number;
  pagination?: boolean;
}

export interface TransferToUser {
  username: string;
}

export interface TransactionsItem {
  uuid?: string | null;
  transferToUser?: TransferToUser | null;
  amount: string;
  trade?: string | null;
  currency: string;
  wallet?: string | null;
  txId?: string | null;
  externalWallet?: string | null;
  isFinalized: boolean;
}

export interface TransactionsResponse {
  totalItems: number;
  transactionsItems: TransactionsItem[];
}
