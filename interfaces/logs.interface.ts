export interface GetLogsParams {
  page?: number;
  itemsPerPage?: number;
  pagination?: boolean;
  createdAtBefore?: string;
  createdAtStrictlyBefore?: string;
  createdAtAfter?: string;
  createdAtStrictlyAfter?: string;
  channel?: string;
  channels?: string;
  level?: number;
  levels?: number[];
  belongToUser?: string;
  belongToUsers?: string;
}

export interface LogsBinanceSearchParams {
  proMerchantAds?: boolean;
  page?: number;
  rows?: number;
  payTypes?: string[];
  countries?: string[];
  publisherType?: string | null;
  fiat?: string;
  tradeType?: string;
  asset?: string;
  merchantCheck?: boolean;
}

export interface LogsContext {
  exception?: string[];
  binanceURL?: string;
  binanceAverageRateMinLiquidity?: number;
  binanceSearchParams?: LogsBinanceSearchParams;
  average_rate?: string;
  rate?: number;
  liquidity?: number;
}

export interface LogsItem {
  id: number;
  channel: string;
  level: string;
  message: string;
  context: LogsContext | any;
  belongToUser?: string;
  extra: string[];
  createdAt: string;
  ctx?: string;
}

export interface LogsResponse {
  totalItems: number;
  logsItems: LogsItem[];
}
