import { AdsDetail } from '@app/interfaces';

export interface GetAdDetailsInputParams {
  page?: number;
  itemsPerPage?: number;
  pagination?: boolean;
}

export interface AdDetailItem extends AdsDetail {
  ad: string;
}

export interface AdDetailsResponse {
  totalItems: number;
  adDetailsItems: AdDetailItem[];
}
