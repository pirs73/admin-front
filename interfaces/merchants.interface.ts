export interface GetMerchantsParams {
  page?: number;
  itemsPerPage?: number;
  pagination?: boolean;
}

export interface MerchantsItem {
  uuid?: string | null;
  name: string;
  user: string;
  discountSellCrypto: string;
  discountBuyCrypto: string;
  isAllowed: boolean;
  isActive: boolean;
  createdAt: string;
}

export interface MerchantsResponse {
  totalItems: number;
  merchantsItems: MerchantsItem[];
}
