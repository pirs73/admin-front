export interface GetWalletsParams {
  page?: number;
  itemsPerPage?: number;
  pagination?: boolean;
  address?: string;
  addresses?: string[];
  user?: string;
  users?: string[];
}

export interface WalletsUser {
  username: string;
}

export interface WalletsCurrency {
  id: string;
  name: string;
}

export interface WalletsItem {
  id: number;
  address: string;
  user?: WalletsUser | null;
  currency: WalletsCurrency;
}

export interface WalletsResponse {
  totalItems: number;
  walletsItems: WalletsItem[];
}
