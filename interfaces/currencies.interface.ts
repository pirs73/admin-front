export interface GetCurrenciesInputParams {
  page?: number;
  itemsPerPage?: number;
  pagination?: boolean;
}

export interface CurrenciesItem {
  id: string;
  name: string;
  symbol: string;
  icon?: string | null;
  isFiat: boolean;
}

export interface CurrenciesResponse {
  totalItems: number;
  currenciesItems: CurrenciesItem[];
}
