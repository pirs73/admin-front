export interface GetApiTokensInputParams {
  page?: number;
  itemsPerPage?: number;
  pagination?: boolean;
}

export interface ApiTokensItem {
  id: string;
  privateKey?: string | null;
  isOnline: boolean;
  name: string;
  createdAt: string;
  lastUsedAt?: string | null;
  belongToUser: string;
}

export interface ApiTokensResponse {
  totalItems: number;
  apiTokensItems: ApiTokensItem[];
}
