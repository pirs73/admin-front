export interface GetReferralsParams {
  page?: number;
  itemsPerPage?: number;
  pagination?: boolean;
}

export interface ReferralsItem {
  uuid?: string | null;
  expireAt?: string | null;
  createdAt: string;
  isMultipleRegistrationsAllowed: boolean;
  isUsed: boolean;
}

export interface ReferralsResponse {
  totalItems: number;
  referralsItems: ReferralsItem[];
}
