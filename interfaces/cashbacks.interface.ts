export interface GetCashbacksInputParams {
  page?: number;
  itemsPerPage?: number;
  pagination?: boolean;
}

export interface CashbacksItem {
  id: number;
  cashbackToUser: string;
  cashbackFromMerchant?: string | null;
  cashbackFromUser?: string | null;
  cashbackSellCrypto: string;
  cashbackBuyCrypto: string;
}

export interface CashbacksResponse {
  totalItems: number;
  cashbacksItems: CashbacksItem[];
}
