export interface GetBalancesInputParams {
  page?: number;
  itemsPerPage?: number;
  pagination?: boolean;
}

export interface BalancesItem {
  id: string;
  currency: string;
  user: string;
  balance?: string | null;
  onHold?: string | null;
}
export interface BalancesResponse {
  totalItems: number;
  balancesItems: BalancesItem[];
}
