export interface GetAdBanksInputParams {
  pagination?: boolean;
}

export interface AdBanksItem {
  name: string;
  showOrder: number;
  shortName: string;
}

export interface AdBanksResponse {
  totalItems: number;
  adBanksItems: AdBanksItem[];
}
