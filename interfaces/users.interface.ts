export interface GetUsersParams {
  orderId: 'ASC' | 'DESC';
  page?: number;
  itemsPerPage?: number;
  pagination?: boolean;
  isOnline?: boolean;
  isActivated?: boolean;
  isTrader?: boolean;
  isMerchant?: boolean;
  createdAtBefore?: string;
  createdAtStrictlyBefore?: string;
  createdAtAfter?: string;
  createdAtStrictlyAfter?: string;
  lastOnlineAtBefore?: string;
  lastOnlineAtStrictlyBefore?: string;
  lastOnlineAtAfter?: string;
  lastOnlineAtStrictlyAfter?: string;
  username?: string;
  usernames?: string[];
}

export interface UsersItem {
  username: string;
  phone?: string | null;
  referredBy?: string | null;
  allowedNumberOfReferrals?: number | null;
  referral?: string | null;
  createdAt: string;
  lastOnlineAt?: string | null;
  isOnline: boolean;
  isActivated: boolean;
  isTrader: boolean;
  isMerchant: boolean;
  scopes?: string[];
}

export interface UsersResponse {
  totalItems: number;
  usersItems: UsersItem[];
}
