/* eslint-disable @typescript-eslint/no-unused-vars */
import { useEffect, useState } from 'react';
import { useRouter } from 'next/navigation';
import Link from 'next/link';
import {
  useLazyViewerQuery,
  useRefreshToken,
  useCheckAccessToken,
} from '@app/apollo/actions';
import { gql, useQuery } from '@apollo/client';
import { Header, SideBar, Spinner } from '@app/components';
import { connectWithSocketServer } from '@app/realtimeCommunication/socketConnection';
import { Viewer } from '@app/interfaces';
import { VIEWER_QUERY } from '@app/apollo/queries';
import { PageLayoutProps } from './PageLayout.props';
import styles from './PageLayout.module.css';

const PageLayout = ({ children, ...props }: PageLayoutProps) => {
  const [viewer, setViewer] = useState<Viewer | null>(null);
  const router = useRouter();
  const [refreshToken] = useRefreshToken();
  const [checkAccessToken] = useCheckAccessToken();
  const [viewerQuery, { data, error, loading }] = useLazyViewerQuery();

  useEffect(() => {
    viewerQuery();
    if (data && data?.viewer) {
      connectWithSocketServer(data.viewer.token);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data]);

  useEffect(() => {
    const getToken = async () => {
      const { data } = await refreshToken({
        refetchQueries: [
          VIEWER_QUERY, // DocumentNode object parsed with gql
        ],
      });

      if (!data?.refreshToken?.jwt_token) {
        router.push('/login');
      }
    };
    const intervalId = setInterval(async () => {
      const { data } = await checkAccessToken();
      // console.log(data);

      if (!data?.checkAccessToken) {
        getToken();
      }
    }, 500);

    return () => clearInterval(intervalId);
    /* eslint-disable react-hooks/exhaustive-deps */
  }, []);

  if (error) {
    return (
      <p>
        {error.message} <Link href="/login">login</Link>
      </p>
    );
  }

  if (loading) {
    return <Spinner />;
  }

  return (
    <div className={styles.pageLayout} {...props}>
      <Header className={styles.header} />
      <SideBar className={styles.sidebar} />
      <main className={styles.body}>{children}</main>
    </div>
  );
};

export { PageLayout };
