import { DetailedHTMLProps, HTMLAttributes, ReactNode } from 'react';

export interface PageLayoutProps
  extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
  children: ReactNode;
  cookies?: any;
}
