import Head from 'next/head';
import cn from 'classnames';
import { BaseLayoutProps } from './BaseLayout.props';
import styles from './BaseLayout.module.css';

const BaseLayout = ({
  children,
  title = '',
  className,
  ...props
}: BaseLayoutProps): JSX.Element => {
  return (
    <>
      <Head>
        <title>{title}</title>
        <meta charSet="utf-8" />
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div className={cn(styles.baseLayout, className)} {...props}>
        {children}
      </div>
    </>
  );
};

export { BaseLayout };
