import { makeVar, InMemoryCache } from '@apollo/client';

export const onlineUsersVar = makeVar([]);

export const newTradeCreatedVar = makeVar([]);

export const cache = new InMemoryCache({
  typePolicies: {
    Query: {
      fields: {
        onlineUsers: {
          read() {
            return onlineUsersVar();
          },
        },
        newTradeCreated: {
          read() {
            return newTradeCreatedVar();
          },
        },
        linkCategories: {
          merge(_existing: any, incoming: any) {
            // Equivalent to what happens if there is no custom merge function.
            return incoming;
          },
        },
      },
    },
  },
});
