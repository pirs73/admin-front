import { useQuery } from '@apollo/client';
import { GET_AD_DETAILS } from '@app/apollo/queries';

export const useGetAdDetails = (options) => useQuery(GET_AD_DETAILS, options);
