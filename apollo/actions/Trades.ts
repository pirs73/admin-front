import { useQuery } from '@apollo/client';
import { GET_TRADES } from '@app/apollo/queries';

export const useGetTrades = (options) => useQuery(GET_TRADES, options);
