import { useQuery } from '@apollo/client';
import { GET_API_TOKENS } from '@app/apollo/queries';

export const useGetApiTokens = (options) => useQuery(GET_API_TOKENS, options);
