import { useQuery } from '@apollo/client';
import { GET_CURRENCY_PAIRS } from '@app/apollo/queries';

export const useGetCurrencyPairs = (options) =>
  useQuery(GET_CURRENCY_PAIRS, options);
