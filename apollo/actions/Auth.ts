import { useLazyQuery, useMutation, useQuery } from '@apollo/client';
import {
  SIGN_IN,
  SIGN_OUT,
  VIEWER_QUERY,
  REFRESH_TOKEN,
  CHECK_ACCESS_TOKEN,
} from '../queries';

export const useSignIn = () =>
  useMutation(SIGN_IN, {
    update(cache: any, { data: { signIn: signedInUser } }: any) {
      cache.writeQuery({
        query: VIEWER_QUERY,
        data: { user: signedInUser },
      });
    },
  });

export const useSignOut = () => useMutation(SIGN_OUT);

export const useGetUser = () => useQuery(VIEWER_QUERY);

export const useLazyViewerQuery = () => useLazyQuery(VIEWER_QUERY);

export const useRefreshToken = () => useMutation(REFRESH_TOKEN);

export const useCheckAccessToken = () => useMutation(CHECK_ACCESS_TOKEN);
