import { useQuery } from '@apollo/client';
import { GET_CASHBACKS } from '@app/apollo/queries';

export const useGetCashbacks = (options) => useQuery(GET_CASHBACKS, options);
