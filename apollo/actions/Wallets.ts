import { useQuery } from '@apollo/client';
import { GET_WALLETS } from '@app/apollo/queries';

export const useGetWallets = (options) => useQuery(GET_WALLETS, options);
