import { useQuery } from '@apollo/client';
import { GET_REFERRALS } from '@app/apollo/queries';

export const useGetReferrals = (options) => useQuery(GET_REFERRALS, options);
