import { useQuery } from '@apollo/client';
import {
  GET_ADS_LIST,
  GET_ADS_DETAILS,
  GET_AD_RESOURCE,
} from '@app/apollo/queries';

export const useGetAdsList = (options) => useQuery(GET_ADS_LIST, options);
export const useGetAdsDetails = (options) => useQuery(GET_ADS_DETAILS, options);
export const useGetAdResource = (options) => useQuery(GET_AD_RESOURCE, options);
