import { useQuery } from '@apollo/client';
import { GET_TRANSACTIONS } from '@app/apollo/queries';

export const useGetTransactions = (options) =>
  useQuery(GET_TRANSACTIONS, options);
