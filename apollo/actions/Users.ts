import { useQuery } from '@apollo/client';
import { GET_USERS } from '@app/apollo/queries';

export const useGetUsers = (options) => useQuery(GET_USERS, options);
