import { useQuery } from '@apollo/client';
import { GET_CURRENCIES } from '@app/apollo/queries';

export const useGetCurrencies = (options) => useQuery(GET_CURRENCIES, options);
