import { useQuery } from '@apollo/client';
import { GET_AD_BANKS } from '@app/apollo/queries';

export const useGetAdBanks = (options) => useQuery(GET_AD_BANKS, options);
