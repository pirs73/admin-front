import { useQuery } from '@apollo/client';
import { GET_BALANCES } from '@app/apollo/queries';

export const useGetBalances = (options) => useQuery(GET_BALANCES, options);
