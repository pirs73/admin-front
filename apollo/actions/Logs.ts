import { useQuery } from '@apollo/client';
import { GET_LOGS } from '@app/apollo/queries';

export const useGetLogs = (options) => useQuery(GET_LOGS, options);
