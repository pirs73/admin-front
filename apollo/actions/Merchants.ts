import { useQuery } from '@apollo/client';
import { GET_MERCHANTS } from '@app/apollo/queries';

export const useGetMerchants = (options) => useQuery(GET_MERCHANTS, options);
