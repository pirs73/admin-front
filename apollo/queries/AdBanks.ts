import { gql } from '@apollo/client';

export const GET_AD_BANKS = gql`
  query AdBanks($pagination: Boolean) {
    adBanks(pagination: $pagination) {
      totalItems
      adBanksItems {
        name
        showOrder
        shortName
      }
    }
  }
`;
