import { gql } from '@apollo/client';

export const GET_BALANCES = gql`
  query Balances($page: Int, $itemsPerPage: Int, $pagination: Boolean) {
    balances(
      page: $page
      itemsPerPage: $itemsPerPage
      pagination: $pagination
    ) {
      totalItems
      balancesItems {
        id
        currency
        user
        balance
        onHold
      }
    }
  }
`;
