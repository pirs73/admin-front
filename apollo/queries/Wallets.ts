import { gql } from '@apollo/client';

export const GET_WALLETS = gql`
  query Wallets($page: Int, $itemsPerPage: Int, $pagination: Boolean) {
    wallets(page: $page, itemsPerPage: $itemsPerPage, pagination: $pagination) {
      totalItems
      walletsItems {
        id
        address
        user {
          username
        }
        currency {
          id
        }
      }
    }
  }
`;
