import { gql } from '@apollo/client';

export const GET_REFERRALS = gql`
  query Referrals($page: Int, $itemsPerPage: Int, $pagination: Boolean) {
    referrals(
      page: $page
      itemsPerPage: $itemsPerPage
      pagination: $pagination
    ) {
      totalItems
      referralsItems {
        uuid
        expireAt
        createdAt
        isMultipleRegistrationsAllowed
        isUsed
      }
    }
  }
`;
