import { gql } from '@apollo/client';

export const GET_USERS = gql`
  query Users(
    $page: Int
    $itemsPerPage: Int
    $pagination: Boolean
    $isOnline: Boolean
    $isActivated: Boolean
    $isTrader: Boolean
    $isMerchant: Boolean
    $createdAtBefore: String
    $createdAtStrictlyBefore: String
    $createdAtAfter: String
    $createdAtStrictlyAfter: String
    $lastOnlineAtBefore: String
    $lastOnlineAtStrictlyBefore: String
    $lastOnlineAtAfter: String
    $lastOnlineAtStrictlyAfter: String
    $username: String
    $usernames: [String]
  ) {
    users(
      page: $page
      itemsPerPage: $itemsPerPage
      pagination: $pagination
      isOnline: $isOnline
      isActivated: $isActivated
      isTrader: $isTrader
      isMerchant: $isMerchant
      createdAtBefore: $createdAtBefore
      createdAtStrictlyBefore: $createdAtStrictlyBefore
      createdAtAfter: $createdAtAfter
      createdAtStrictlyAfter: $createdAtStrictlyAfter
      lastOnlineAtBefore: $lastOnlineAtBefore
      lastOnlineAtStrictlyBefore: $lastOnlineAtStrictlyBefore
      lastOnlineAtAfter: $lastOnlineAtAfter
      lastOnlineAtStrictlyAfter: $lastOnlineAtStrictlyAfter
      username: $username
      usernames: $usernames
    ) {
      totalItems
      usersItems {
        username
        phone
        referredBy
        allowedNumberOfReferrals
        referral
        createdAt
        lastOnlineAt
        isOnline
        isActivated
        isTrader
        isMerchant
        scopes
      }
    }
  }
`;
