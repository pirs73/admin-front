import { gql } from '@apollo/client';

export const GET_API_TOKENS = gql`
  query ApiTokens($page: Int, $itemsPerPage: Int, $pagination: Boolean) {
    apiTokens(
      page: $page
      itemsPerPage: $itemsPerPage
      pagination: $pagination
    ) {
      totalItems
      apiTokensItems {
        id
        privateKey
        isOnline
        name
        createdAt
        lastUsedAt
      }
    }
  }
`;
