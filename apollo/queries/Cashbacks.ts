import { gql } from '@apollo/client';

export const GET_CASHBACKS = gql`
  query Cashbacks($page: Int, $itemsPerPage: Int, $pagination: Boolean) {
    cashbacks(
      page: $page
      itemsPerPage: $itemsPerPage
      pagination: $pagination
    ) {
      totalItems
      cashbacksItems {
        id
        cashbackToUser
        cashbackFromMerchant
        cashbackFromUser
        cashbackSellCrypto
        cashbackBuyCrypto
      }
    }
  }
`;
