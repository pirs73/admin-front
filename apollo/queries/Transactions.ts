import { gql } from '@apollo/client';

export const GET_TRANSACTIONS = gql`
  query Transactions($page: Int, $itemsPerPage: Int, $pagination: Boolean) {
    transactions(
      page: $page
      itemsPerPage: $itemsPerPage
      pagination: $pagination
    ) {
      totalItems
      transactionsItems {
        uuid
        transferToUser {
          username
        }
        amount
        trade
        currency
        wallet
        txId
        externalWallet
        isFinalized
      }
    }
  }
`;
