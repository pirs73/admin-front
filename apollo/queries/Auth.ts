import { gql } from '@apollo/client';

export const SIGN_UP = gql`
  mutation SignUp(
    $username: String!
    $password: String!
    $referralCode: String!
  ) {
    signUp(
      input: {
        username: $username
        password: $password
        referralCode: $referralCode
      }
    )
  }
`;

export const SIGN_IN = gql`
  mutation SignIn($username: String!, $password: String!) {
    signIn(input: { username: $username, password: $password }) {
      token
    }
  }
`;

export const SIGN_OUT = gql`
  mutation SignOut {
    signOut
  }
`;

export const REFRESH_TOKEN = gql`
  mutation RefreshToken {
    refreshToken {
      jwt_token
    }
  }
`;

export const CHECK_ACCESS_TOKEN = gql`
  mutation CheckAccessToken {
    checkAccessToken
  }
`;

export const VIEWER_QUERY = gql`
  query Viewer {
    viewer {
      token
      viewer {
        username
        scopes
      }
    }
  }
`;
