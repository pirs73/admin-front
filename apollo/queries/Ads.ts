import { gql } from '@apollo/client';

export const GET_ADS_LIST = gql`
  query AdsList(
    $page: Int
    $itemsPerPage: Int
    $pagination: Boolean
    $isActive: Boolean
    $currencyPair: String
    $currencyPairs: [String]
    $bank: String
    $banks: [String]
    $user: String
    $users: [String]
  ) {
    adsList(
      page: $page
      itemsPerPage: $itemsPerPage
      pagination: $pagination
      isActive: $isActive
      currencyPair: $currencyPair
      currencyPairs: $currencyPairs
      bank: $bank
      banks: $banks
      user: $user
      users: $users
    ) {
      totalItems
      adsItems {
        uuid
        currencyPair {
          name
        }
        minAmount
        maxAmount
        user
        details {
          text
          number
          transactionSum
          uuid
        }
        isActive
        bank {
          name
          shortName
        }
      }
    }
  }
`;

export const GET_ADS_DETAILS = gql`
  query AdsDetails {
    adsList {
      details {
        text
        number
        transactionSum
        uuid
      }
    }
  }
`;

export const GET_AD_RESOURCE = gql`
  query GetAdResource($uuid: String!) {
    getAdResource(uuid: $uuid) {
      uuid
      currencyPair {
        name
      }
      minAmount
      maxAmount
      user
      details {
        text
        number
        transactionSum
        uuid
      }
      isActive
      bank {
        name
        shortName
      }
    }
  }
`;
