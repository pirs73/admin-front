import { gql } from '@apollo/client';

export const GET_CURRENCY_PAIRS = gql`
  query CurrencyPairs($page: Int, $itemsPerPage: Int, $pagination: Boolean) {
    currencyPairs(
      page: $page
      itemsPerPage: $itemsPerPage
      pagination: $pagination
    ) {
      totalItems
      currencyPairsItems {
        name
        currencyFrom
        currencyTo
        exchangeRate
      }
    }
  }
`;
