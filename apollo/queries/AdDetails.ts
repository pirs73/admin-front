import { gql } from '@apollo/client';

export const GET_AD_DETAILS = gql`
  query AdDetails($page: Int, $itemsPerPage: Int, $pagination: Boolean) {
    adDetails(
      page: $page
      itemsPerPage: $itemsPerPage
      pagination: $pagination
    ) {
      totalItems
      adDetailsItems {
        uuid
        ad
        number
        text
        transactionSum
      }
    }
  }
`;
