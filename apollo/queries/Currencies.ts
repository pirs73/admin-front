import { gql } from '@apollo/client';

export const GET_CURRENCIES = gql`
  query Currencies($page: Int, $itemsPerPage: Int, $pagination: Boolean) {
    currencies(
      page: $page
      itemsPerPage: $itemsPerPage
      pagination: $pagination
    ) {
      totalItems
      currenciesItems {
        id
        name
        symbol
        icon
        isFiat
      }
    }
  }
`;
