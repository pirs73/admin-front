import { gql } from '@apollo/client';

export const GET_MERCHANTS = gql`
  query Merchants($page: Int, $itemsPerPage: Int, $pagination: Boolean) {
    merchants(
      page: $page
      itemsPerPage: $itemsPerPage
      pagination: $pagination
    ) {
      totalItems
      merchantsItems {
        uuid
        name
        user
        discountSellCrypto
        discountBuyCrypto
        isAllowed
        isActive
        createdAt
      }
    }
  }
`;
