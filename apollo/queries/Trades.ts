import { gql } from '@apollo/client';

export const GET_TRADES = gql`
  query Trades(
    $page: Int
    $itemsPerPage: Int
    $pagination: Boolean
    $order: CreatedAt
    $isCanceled: Boolean
    $isPaymentSend: Boolean
    $isOnDispute: Boolean
    $isConfirmed: Boolean
  ) {
    trades(
      page: $page
      itemsPerPage: $itemsPerPage
      pagination: $pagination
      order: $order
      isCanceled: $isCanceled
      isPaymentSend: $isPaymentSend
      isOnDispute: $isOnDispute
      isConfirmed: $isConfirmed
    ) {
      totalItems
      tradesItems {
        uuid
        currencyPair
        amount
        rate
        createdAt
        completedAt
        user
        adDetail
        isConfirmed
        isOnDispute
        isPaymentSend
        isCanceled
      }
    }
  }
`;
