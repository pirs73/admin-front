import { gql } from '@apollo/client';

export const GET_LOGS = gql`
  query Logs(
    $page: Int
    $itemsPerPage: Int
    $pagination: Boolean
    $createdAtBefore: String
    $createdAtStrictlyBefore: String
    $createdAtAfter: String
    $createdAtStrictlyAfter: String
    $belongToUser: String
    $belongToUsers: [String]
  ) {
    logs(
      page: $page
      itemsPerPage: $itemsPerPage
      pagination: $pagination
      createdAtBefore: $createdAtBefore
      createdAtStrictlyBefore: $createdAtStrictlyBefore
      createdAtAfter: $createdAtAfter
      createdAtStrictlyAfter: $createdAtStrictlyAfter
      belongToUser: $belongToUser
      belongToUsers: $belongToUsers
    ) {
      totalItems
      logsItems {
        id
        channel
        level
        message
        context {
          exception
          binanceURL
          binanceAverageRateMinLiquidity
          binanceSearchParams {
            proMerchantAds
            page
            rows
            payTypes
            countries
            publisherType
            fiat
            tradeType
            asset
            merchantCheck
          }
          average_rate
          rate
          liquidity
        }
        belongToUser
        extra
        createdAt
      }
    }
  }
`;
