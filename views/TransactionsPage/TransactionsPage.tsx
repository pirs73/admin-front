import { useEffect, useState } from 'react';
import Link from 'next/link';
import { Button, Pagination, Stack } from '@mui/material';
import AddIcon from '@mui/icons-material/Add';
import EditIcon from '@mui/icons-material/Edit';
import PreviewIcon from '@mui/icons-material/Preview';
import CheckIcon from '@mui/icons-material/Check';
import CloseIcon from '@mui/icons-material/Close';
import { useGetTransactions } from '@app/apollo/actions';
import { TransactionsItem, TransactionsResponse } from '@app/interfaces';

const useInitialData = (
  page,
  itemsPerPage,
  pagination,
): TransactionsResponse => {
  const { data } = useGetTransactions({
    variables: {
      page,
      itemsPerPage,
      pagination,
    },
  });

  const totalItems =
    (data && data.transactions && data.transactions.totalItems) || 0;
  const transactionsItems: TransactionsItem[] =
    (data && data.transactions && data.transactions.transactionsItems) || [];

  return {
    totalItems,
    transactionsItems,
  };
};

const TransactionsPage = () => {
  const [page, setPage] = useState(1);
  const [itemsPerPage, setItemsPerPage] = useState<number>(20);
  const [pagesAll, setPagesAll] = useState(0);

  const { transactionsItems: data, totalItems } = useInitialData(
    page,
    itemsPerPage,
    true,
  );

  useEffect(() => {
    const numPages = Math.ceil(totalItems / itemsPerPage);
    setPagesAll(numPages);
  }, [itemsPerPage, totalItems]);

  const handleChangePage = (page: number) => {
    setPage(page);
    const isBrowser = typeof window !== 'undefined';
    if (isBrowser) {
      window.scrollTo({ top: 0, behavior: 'smooth' });
    }
  };

  return (
    <>
      <h1>Transactions</h1>
      <div className="top_buttons_container">
        <Button
          href="/transactions/create/"
          variant="outlined"
          startIcon={<AddIcon />}
        >
          Create
        </Button>
      </div>
      <div className="table_container">
        <table className="table">
          <thead>
            <tr>
              <td className="td">Transfer To User</td>
              <td className="td">Amount</td>
              <td className="td">Trade</td>
              <td className="td">Currency</td>
              <td className="td">Wallet</td>
              <td className="td">Tx Id</td>
              <td className="td">External Wallet</td>
              <td className="td">Is Finalized</td>
              <td colSpan={2}></td>
            </tr>
          </thead>
          <tbody>
            {!data ||
              (data.length === 0 && (
                <tr>
                  <td
                    className="td"
                    colSpan={7}
                    style={{ textAlign: 'center' }}
                  >
                    <p className="text_not_found">Noting Found</p>
                  </td>
                </tr>
              ))}
            {data &&
              data.length > 0 &&
              data.map((d, index) => (
                <tr key={`${d.uuid}-${index}`}>
                  <td className="td">
                    {d.transferToUser && d.transferToUser.username}
                  </td>
                  <td className="td">{d.amount}</td>
                  <td className="td">
                    <Link href={`/trades/${d.trade}`}>{d.trade}</Link>
                  </td>
                  <td className="td">{d.currency}</td>
                  <td className="td">{d.wallet}</td>
                  <td className="td">{d.txId}</td>
                  <td className="td">{d.externalWallet}</td>
                  <td className="td">
                    {d.isFinalized ? <CheckIcon /> : <CloseIcon />}
                  </td>
                  <td className="td">
                    {d.uuid && (
                      <Button
                        variant="contained"
                        href={`/transactions/${d.uuid}`}
                        startIcon={<PreviewIcon />}
                      >
                        Show
                      </Button>
                    )}
                  </td>
                  <td className="td">
                    {d.uuid && (
                      <Button
                        variant="contained"
                        color="warning"
                        href={`/transactions/${d.uuid}/edit`}
                        startIcon={<EditIcon />}
                      >
                        Edit
                      </Button>
                    )}
                  </td>
                </tr>
              ))}
          </tbody>
        </table>
        {data && data.length > 0 && (
          <Stack spacing={2} className="pagination_container">
            <p className="pagination_container__p">Rows per page:</p>
            <select
              onChange={(e) => setItemsPerPage(Number(e.target.value))}
              style={{ marginTop: '0px', marginLeft: '16px' }}
            >
              <option value={20}>20</option>
              <option value={50}>50</option>
              <option value={100}>100</option>
            </select>
            <Pagination
              style={{ marginTop: '0px' }}
              count={pagesAll ? pagesAll : 0}
              showFirstButton
              showLastButton
              onChange={(_e, page: number) => handleChangePage(page)}
            />
          </Stack>
        )}
      </div>
    </>
  );
};

export default TransactionsPage;
