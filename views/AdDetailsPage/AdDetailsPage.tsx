import { memo, useEffect, useState } from 'react';
import Link from 'next/link';
import { Button, Pagination, Stack } from '@mui/material';
import PreviewIcon from '@mui/icons-material/Preview';
import { useGetAdDetails } from '@app/apollo/actions';
import { AdDetailItem, AdDetailsResponse } from '@app/interfaces';

const useInitialData = (page, itemsPerPage, pagination): AdDetailsResponse => {
  const { data } = useGetAdDetails({
    variables: {
      page,
      itemsPerPage,
      pagination,
    },
  });

  const totalItems = (data && data.adDetails && data.adDetails.totalItems) || 0;
  const adDetailsItems: AdDetailItem[] =
    (data && data.adDetails && data.adDetails.adDetailsItems) || [];

  return {
    totalItems,
    adDetailsItems,
  };
};

const AdDetailsPage = () => {
  const [page, setPage] = useState(1);
  const [itemsPerPage, setItemsPerPage] = useState<number>(20);
  const [pagesAll, setPagesAll] = useState(0);

  const pagination = true;

  const { adDetailsItems: data, totalItems } = useInitialData(
    page,
    itemsPerPage,
    pagination,
  );

  useEffect(() => {
    const numPages = Math.ceil(totalItems / itemsPerPage);
    setPagesAll(numPages);
  }, [itemsPerPage, totalItems]);

  const handleChangePage = (page: number) => {
    setPage(page);
    const isBrowser = typeof window !== 'undefined';
    if (isBrowser) {
      window.scrollTo({ top: 0, behavior: 'smooth' });
    }
  };

  return (
    <>
      <h1>Ads Details</h1>
      <div className="table_container">
        <table className="table">
          <thead>
            <tr>
              <td className="td">Ad ID</td>
              <td className="td">Number</td>
              <td className="td">Text</td>
              <td className="td">Transaction sum</td>
              <td className="td"></td>
            </tr>
          </thead>
          <tbody>
            {data &&
              data.length > 0 &&
              data.map((d) => (
                <tr key={d.uuid}>
                  <td className="td">
                    <Link href={`/ads/${d.ad}`}>{d.ad}</Link>
                  </td>
                  <td className="td">{d.number}</td>
                  <td className="td">{d.text}</td>
                  <td className="td">{d.transactionSum}</td>
                  <td className="td">
                    {d.uuid && (
                      <Button
                        variant="contained"
                        href={`/ad_details/${d.uuid}`}
                        startIcon={<PreviewIcon />}
                      >
                        Show
                      </Button>
                    )}
                  </td>
                </tr>
              ))}
          </tbody>
        </table>
        {data && data.length > 0 && (
          <Stack spacing={2} className="pagination_container">
            <p className="pagination_container__p">Rows per page:</p>
            <select
              onChange={(e) => setItemsPerPage(Number(e.target.value))}
              style={{ marginTop: '0px', marginLeft: '16px' }}
            >
              <option value={20}>20</option>
              <option value={50}>50</option>
              <option value={100}>100</option>
            </select>
            <Pagination
              style={{ marginTop: '0px' }}
              count={pagesAll ? pagesAll : 0}
              showFirstButton
              showLastButton
              onChange={(_e, page: number) => handleChangePage(page)}
            />
          </Stack>
        )}
      </div>
    </>
  );
};

export default memo(AdDetailsPage);
