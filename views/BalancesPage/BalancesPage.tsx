import { memo, useEffect, useState } from 'react';
import Link from 'next/link';
import { Button, Pagination, Stack } from '@mui/material';
import PreviewIcon from '@mui/icons-material/Preview';
import { useGetBalances } from '@app/apollo/actions';
import { BalancesItem, BalancesResponse } from '@app/interfaces';

const useInitialData = (page, itemsPerPage, pagination): BalancesResponse => {
  const { data } = useGetBalances({
    variables: {
      page,
      itemsPerPage,
      pagination,
    },
  });

  const totalItems = (data && data.balances && data.balances.totalItems) || 0;
  const balancesItems: BalancesItem[] =
    (data && data.balances && data.balances.balancesItems) || [];

  return {
    totalItems,
    balancesItems,
  };
};

const BalancesPage = () => {
  const [page, setPage] = useState(1);
  const [itemsPerPage, setItemsPerPage] = useState<number>(20);
  const [pagesAll, setPagesAll] = useState(0);

  const { balancesItems: data, totalItems } = useInitialData(
    page,
    itemsPerPage,
    true,
  );

  useEffect(() => {
    const numPages = Math.ceil(totalItems / itemsPerPage);
    setPagesAll(numPages);
  }, [itemsPerPage, totalItems]);

  const handleChangePage = (page: number) => {
    setPage(page);
    const isBrowser = typeof window !== 'undefined';
    if (isBrowser) {
      window.scrollTo({ top: 0, behavior: 'smooth' });
    }
  };

  return (
    <>
      <h1>Balances</h1>
      <div className="table_container">
        <table className="table">
          <thead>
            <tr>
              <td className="td">Currency</td>
              <td className="td">User</td>
              <td className="td">Balance</td>
              <td className="td">On Hold</td>
              <td className="td"></td>
            </tr>
          </thead>
          <tbody>
            {data &&
              data.length > 0 &&
              data.map((d) => (
                <tr key={d.id}>
                  <td className="td">{d.currency}</td>
                  <td className="td">
                    <Link href={`/users/${d.user}`}>{d.user}</Link>
                  </td>
                  <td className="td">{d.balance ? d.balance : ''}</td>
                  <td className="td">{d.onHold ? d.onHold : ''}</td>
                  <td className="td">
                    <Button
                      variant="contained"
                      href={`/balances/${d.id}`}
                      startIcon={<PreviewIcon />}
                    >
                      Show
                    </Button>
                  </td>
                </tr>
              ))}
          </tbody>
        </table>
        {data && data.length > 0 && (
          <Stack spacing={2} className="pagination_container">
            <p className="pagination_container__p">Rows per page:</p>
            <select
              onChange={(e) => setItemsPerPage(Number(e.target.value))}
              style={{ marginTop: '0px', marginLeft: '16px' }}
            >
              <option value={20}>20</option>
              <option value={50}>50</option>
              <option value={100}>100</option>
            </select>
            <Pagination
              style={{ marginTop: '0px' }}
              count={pagesAll ? pagesAll : 0}
              showFirstButton
              showLastButton
              onChange={(_e, page: number) => handleChangePage(page)}
            />
          </Stack>
        )}
      </div>
    </>
  );
};

export default memo(BalancesPage);
