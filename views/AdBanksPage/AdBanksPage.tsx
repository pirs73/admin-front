import { memo } from 'react';
import { Button } from '@mui/material';
import AddIcon from '@mui/icons-material/Add';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import PreviewIcon from '@mui/icons-material/Preview';
import { useGetAdBanks } from '@app/apollo/actions';
import { AdBanksItem, AdBanksResponse } from '@app/interfaces';

const useInitialData = (pagination): AdBanksResponse => {
  const { data } = useGetAdBanks({
    variables: {
      pagination,
    },
  });

  const totalItems = (data && data.adBanks && data.adBanks.totalItems) || 0;
  const adBanksItems: AdBanksItem[] =
    (data && data.adBanks && data.adBanks.adBanksItems) || [];

  return {
    totalItems,
    adBanksItems,
  };
};

const AdBanksPage = () => {
  const { adBanksItems: data } = useInitialData(false);

  return (
    <>
      <h1>Ad Banks</h1>
      <div className="top_buttons_container">
        <Button
          href="/ad_banks/create/"
          variant="outlined"
          startIcon={<AddIcon />}
        >
          Create
        </Button>
      </div>
      <div className="table_container">
        <table className="table">
          <thead>
            <tr>
              <td className="td">Name</td>
              <td className="td">Show order</td>
              <td className="td">Short Name</td>
              <td colSpan={3} className="td"></td>
            </tr>
          </thead>
          <tbody>
            {data &&
              data.length > 0 &&
              data.map((d) => (
                <tr key={d.shortName}>
                  <td className="td">{d.name}</td>
                  <td className="td">{d.showOrder}</td>
                  <td className="td">{d.shortName}</td>
                  <td className="td">
                    <Button
                      variant="contained"
                      href={`/ad_banks/${d.shortName}`}
                      startIcon={<PreviewIcon />}
                    >
                      Show
                    </Button>
                  </td>
                  <td className="td">
                    <Button
                      variant="contained"
                      color="warning"
                      href={`/ad_banks/${d.shortName}/edit`}
                      startIcon={<EditIcon />}
                    >
                      Edit
                    </Button>
                  </td>
                  <td className="td">
                    <Button
                      variant="contained"
                      color="error"
                      startIcon={<DeleteIcon />}
                      onClick={() => confirm(`delete ad bank #${d.shortName}`)}
                    >
                      Delete
                    </Button>
                  </td>
                </tr>
              ))}
          </tbody>
        </table>
      </div>
    </>
  );
};

export default memo(AdBanksPage);
