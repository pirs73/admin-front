import { useEffect, useState } from 'react';
import { Button, Pagination, Stack } from '@mui/material';
import AddIcon from '@mui/icons-material/Add';
import DeleteIcon from '@mui/icons-material/Delete';
import PreviewIcon from '@mui/icons-material/Preview';
import CheckIcon from '@mui/icons-material/Check';
import CloseIcon from '@mui/icons-material/Close';
import { useGetReferrals } from '@app/apollo/actions';
import { ReferralsItem, ReferralsResponse } from '@app/interfaces';

const useInitialData = (page, itemsPerPage, pagination): ReferralsResponse => {
  const { data } = useGetReferrals({
    variables: {
      page,
      itemsPerPage,
      pagination,
    },
  });

  const totalItems = (data && data.referrals && data.referrals.totalItems) || 0;
  const referralsItems: ReferralsItem[] =
    (data && data.referrals && data.referrals.referralsItems) || [];

  return {
    totalItems,
    referralsItems,
  };
};

const ReferralsPage = () => {
  const [page, setPage] = useState(1);
  const [itemsPerPage, setItemsPerPage] = useState<number>(20);
  const [pagesAll, setPagesAll] = useState(0);

  const { referralsItems: data, totalItems } = useInitialData(
    page,
    itemsPerPage,
    true,
  );

  useEffect(() => {
    const numPages = Math.ceil(totalItems / itemsPerPage);
    setPagesAll(numPages);
  }, [itemsPerPage, totalItems]);

  const handleChangePage = (page: number) => {
    setPage(page);
    const isBrowser = typeof window !== 'undefined';
    if (isBrowser) {
      window.scrollTo({ top: 0, behavior: 'smooth' });
    }
  };

  return (
    <>
      <h1>Referrals</h1>
      <div className="top_buttons_container">
        <Button
          href="/referrals/create/"
          variant="outlined"
          startIcon={<AddIcon />}
        >
          Create
        </Button>
      </div>
      <div className="table_container">
        <table className="table">
          <thead>
            <tr>
              <td className="td">Created At</td>
              <td className="td">Expire At</td>
              <td className="td">Is Multiple Registrations Allowed</td>
              <td className="td">Is Used</td>
              <td className="td" colSpan={2}>
                show
              </td>
            </tr>
          </thead>
          <tbody>
            {!data ||
              (data.length === 0 && (
                <tr>
                  <td
                    className="td"
                    colSpan={6}
                    style={{ textAlign: 'center' }}
                  >
                    <p className="text_not_found">Noting Found</p>
                  </td>
                </tr>
              ))}
            {data &&
              data.length > 0 &&
              data.map((d, index) => (
                <tr key={`${d.uuid}-${index}`}>
                  <td className="td">{d.createdAt}</td>
                  <td className="td">{d.expireAt}</td>
                  <td className="td">
                    {d.isMultipleRegistrationsAllowed ? (
                      <CheckIcon />
                    ) : (
                      <CloseIcon />
                    )}
                  </td>
                  <td className="td">
                    {d.isUsed ? <CheckIcon /> : <CloseIcon />}
                  </td>
                  <td className="td">
                    {d.uuid && (
                      <Button
                        variant="contained"
                        href={`/referrals/${d.uuid}`}
                        startIcon={<PreviewIcon />}
                      >
                        Show
                      </Button>
                    )}
                  </td>
                  <td className="td">
                    {d.uuid && (
                      <Button
                        variant="contained"
                        color="error"
                        startIcon={<DeleteIcon />}
                        onClick={() => confirm(`delete ref #${d.uuid}`)}
                      >
                        Delete
                      </Button>
                    )}
                  </td>
                </tr>
              ))}
          </tbody>
        </table>
        {data && data.length > 0 && (
          <Stack spacing={2} className="pagination_container">
            <p className="pagination_container__p">Rows per page:</p>
            <select
              onChange={(e) => setItemsPerPage(Number(e.target.value))}
              style={{ marginTop: '0px', marginLeft: '16px' }}
            >
              <option value={20}>20</option>
              <option value={50}>50</option>
              <option value={100}>100</option>
            </select>
            <Pagination
              style={{ marginTop: '0px' }}
              count={pagesAll ? pagesAll : 0}
              showFirstButton
              showLastButton
              onChange={(_e, page: number) => handleChangePage(page)}
            />
          </Stack>
        )}
      </div>
    </>
  );
};

export default ReferralsPage;
