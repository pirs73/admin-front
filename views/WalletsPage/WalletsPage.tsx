import { useEffect, useState } from 'react';
import Link from 'next/link';
import { Button, Pagination, Stack } from '@mui/material';
import AddIcon from '@mui/icons-material/Add';
import DeleteIcon from '@mui/icons-material/Delete';
import PreviewIcon from '@mui/icons-material/Preview';
import { useGetWallets } from '@app/apollo/actions';
import { WalletsItem, WalletsResponse } from '@app/interfaces';

const useInitialData = (page, itemsPerPage, pagination): WalletsResponse => {
  const { data } = useGetWallets({
    variables: {
      page,
      itemsPerPage,
      pagination,
    },
  });

  const totalItems = (data && data.wallets && data.wallets.totalItems) || 0;
  const walletsItems: WalletsItem[] =
    (data && data.wallets && data.wallets.walletsItems) || [];

  return {
    totalItems,
    walletsItems,
  };
};

const WalletsPage = () => {
  const [page, setPage] = useState(1);
  const [itemsPerPage, setItemsPerPage] = useState<number>(20);
  const [pagesAll, setPagesAll] = useState(0);

  const { walletsItems: data, totalItems } = useInitialData(
    page,
    itemsPerPage,
    true,
  );

  useEffect(() => {
    const numPages = Math.ceil(totalItems / itemsPerPage);
    setPagesAll(numPages);
  }, [itemsPerPage, totalItems]);

  const handleChangePage = (page: number) => {
    setPage(page);
    const isBrowser = typeof window !== 'undefined';
    if (isBrowser) {
      window.scrollTo({ top: 0, behavior: 'smooth' });
    }
  };

  return (
    <>
      <h1>Wallets</h1>
      <div className="top_buttons_container">
        <Button
          href="/wallets/create/"
          variant="outlined"
          startIcon={<AddIcon />}
        >
          Create
        </Button>
      </div>
      <div className="table_container">
        <table className="table">
          <thead>
            <tr>
              <td className="td">Address</td>
              <td className="td">Username</td>
              <td className="td">Currency</td>
              <td className="td" colSpan={2}></td>
            </tr>
          </thead>
          <tbody>
            {!data ||
              (data.length === 0 && (
                <tr>
                  <td
                    className="td"
                    colSpan={5}
                    style={{ textAlign: 'center' }}
                  >
                    <p className="text_not_found">Noting Found</p>
                  </td>
                </tr>
              ))}
            {data &&
              data.length > 0 &&
              data.map((d, index) => (
                <tr key={`${d.id}-${index}`}>
                  <td className="td">
                    <Link href={`/wallets/${d.address}`}>{d.address}</Link>
                  </td>
                  <td className="td">{d.user && d.user.username}</td>
                  <td className="td">{d.currency.id}</td>
                  <td className="td">
                    <Button
                      variant="contained"
                      href={`/wallets/${d.address}`}
                      startIcon={<PreviewIcon />}
                    >
                      Show
                    </Button>
                  </td>
                  <td className="td">
                    <Button
                      variant="contained"
                      color="error"
                      startIcon={<DeleteIcon />}
                      onClick={() => confirm(`delete wallet #${d.address}`)}
                    >
                      Delete
                    </Button>
                  </td>
                </tr>
              ))}
          </tbody>
        </table>
        {data && data.length > 0 && (
          <Stack spacing={2} className="pagination_container">
            <p className="pagination_container__p">Rows per page:</p>
            <select
              onChange={(e) => setItemsPerPage(Number(e.target.value))}
              style={{ marginTop: '0px', marginLeft: '16px' }}
            >
              <option value={20}>20</option>
              <option value={50}>50</option>
              <option value={100}>100</option>
            </select>
            <Pagination
              style={{ marginTop: '0px' }}
              count={pagesAll ? pagesAll : 0}
              showFirstButton
              showLastButton
              onChange={(_e, page: number) => handleChangePage(page)}
            />
          </Stack>
        )}
      </div>
    </>
  );
};

export default WalletsPage;
