import { useEffect, useState } from 'react';
import Link from 'next/link';
import { Button, Pagination, Stack } from '@mui/material';
import AddIcon from '@mui/icons-material/Add';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import PreviewIcon from '@mui/icons-material/Preview';
import CheckIcon from '@mui/icons-material/Check';
import CloseIcon from '@mui/icons-material/Close';
import { useGetAdsList } from '@app/apollo/actions';
import { AdsListItem, AdsResponse } from '@app/interfaces';

const useInitialData = (page, itemsPerPage, pagination): AdsResponse => {
  const { data } = useGetAdsList({
    variables: {
      page,
      itemsPerPage,
      pagination,
    },
  });

  const totalItems = (data && data.adsList && data.adsList.totalItems) || 0;
  const adsItems: AdsListItem[] =
    (data && data.adsList && data.adsList.adsItems) || [];

  return {
    totalItems,
    adsItems,
  };
};

const AdsPage = () => {
  const [page, setPage] = useState(1);
  const [itemsPerPage, setItemsPerPage] = useState<number>(20);
  const [pagesAll, setPagesAll] = useState(0);

  const { adsItems: data, totalItems } = useInitialData(
    page,
    itemsPerPage,
    true,
  );

  useEffect(() => {
    const numPages = Math.ceil(totalItems / itemsPerPage);
    setPagesAll(numPages);
  }, [itemsPerPage, totalItems]);

  const handleChangePage = (page: number) => {
    setPage(page);
    const isBrowser = typeof window !== 'undefined';
    if (isBrowser) {
      window.scrollTo({ top: 0, behavior: 'smooth' });
    }
  };

  return (
    <>
      <h1>Adverts</h1>
      <div className="top_buttons_container">
        <Button href="/ads/create/" variant="outlined" startIcon={<AddIcon />}>
          Create
        </Button>
      </div>
      <div className="table_container">
        <table className="table">
          <thead>
            <tr>
              <td className="td">CurrencyPair</td>
              <td className="td">User</td>
              <td className="td">Min amount</td>
              <td className="td">Max amount</td>
              <td className="td">Details</td>
              <td className="td">Is Active</td>
              <td className="td">Bank</td>
              <td colSpan={3} className="td"></td>
            </tr>
          </thead>
          <tbody>
            {!data ||
              (data.length === 0 && (
                <tr>
                  <td
                    className="td"
                    colSpan={7}
                    style={{ textAlign: 'center' }}
                  >
                    <p className="text_not_found">Noting Found</p>
                  </td>
                </tr>
              ))}
            {data &&
              data.length > 0 &&
              data.map((ads, index) => (
                <tr key={`${ads.uuid}-${index}`}>
                  <td className="td">{ads.currencyPair.name}</td>
                  <td className="td">
                    <Link href={`/users/${ads.user}`}>{ads.user}</Link>
                  </td>
                  <td className="td">{ads.minAmount}</td>
                  <td className="td">{ads.maxAmount}</td>
                  <td className="td">
                    {ads.details &&
                      ads.details.length > 0 &&
                      ads.details.map((d) => (
                        <span key={d.uuid}>{d.number} &nbsp;</span>
                      ))}
                  </td>
                  <td className="td">
                    {ads.isActive ? <CheckIcon /> : <CloseIcon />}
                  </td>
                  <td className="td">{ads.bank.name}</td>
                  <td className="td">
                    {ads.uuid && (
                      <Button
                        variant="contained"
                        href={`/ads/${ads.uuid}`}
                        startIcon={<PreviewIcon />}
                      >
                        Show
                      </Button>
                    )}
                  </td>
                  <td className="td">
                    {ads.uuid && (
                      <Button
                        variant="contained"
                        color="warning"
                        href={`/ads/${ads.uuid}/edit`}
                        startIcon={<EditIcon />}
                      >
                        Edit
                      </Button>
                    )}
                  </td>
                  <td className="td">
                    {ads.uuid && (
                      <Button
                        variant="contained"
                        color="error"
                        startIcon={<DeleteIcon />}
                        onClick={() => confirm(`delete ad #${ads.uuid}`)}
                      >
                        Delete
                      </Button>
                    )}
                  </td>
                </tr>
              ))}
          </tbody>
        </table>
        {data && data.length > 0 && (
          <Stack spacing={2} className="pagination_container">
            <p className="pagination_container__p">Rows per page:</p>
            <select
              onChange={(e) => setItemsPerPage(Number(e.target.value))}
              style={{ marginTop: '0px', marginLeft: '16px' }}
            >
              <option value={20}>20</option>
              <option value={50}>50</option>
              <option value={100}>100</option>
            </select>
            <Pagination
              style={{ marginTop: '0px' }}
              count={pagesAll ? pagesAll : 0}
              showFirstButton
              showLastButton
              onChange={(_e, page: number) => handleChangePage(page)}
            />
          </Stack>
        )}
      </div>
    </>
  );
};

export default AdsPage;
