import { useEffect, useState } from 'react';
import { Button, Pagination, Stack } from '@mui/material';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import PreviewIcon from '@mui/icons-material/Preview';
import CheckIcon from '@mui/icons-material/Check';
import CloseIcon from '@mui/icons-material/Close';
import { useGetUsers } from '@app/apollo/actions';
import { UsersItem, UsersResponse } from '@app/interfaces';

const useInitialData = (page, itemsPerPage, pagination): UsersResponse => {
  const { data } = useGetUsers({
    variables: {
      page,
      itemsPerPage,
      pagination,
    },
  });

  const totalItems = (data && data.users && data.users.totalItems) || 0;
  const usersItems: UsersItem[] =
    (data && data.users && data.users.usersItems) || [];

  return {
    totalItems,
    usersItems,
  };
};

const UsersPage = () => {
  const [page, setPage] = useState(1);
  const [itemsPerPage, setItemsPerPage] = useState<number>(20);
  const [pagesAll, setPagesAll] = useState(0);
  const { usersItems: data, totalItems } = useInitialData(
    page,
    itemsPerPage,
    true,
  );

  useEffect(() => {
    const numPages = Math.ceil(totalItems / itemsPerPage);
    setPagesAll(numPages);
  }, [itemsPerPage, totalItems]);

  const handleChangePage = (page: number) => {
    setPage(page);
    const isBrowser = typeof window !== 'undefined';
    if (isBrowser) {
      window.scrollTo({ top: 0, behavior: 'smooth' });
    }
  };

  return (
    <>
      <h1>Users</h1>
      <div className="table_container">
        <table className="table">
          <thead>
            <tr>
              <td className="td">is Activated</td>
              <td className="td">Username</td>
              <td className="td">is Trader</td>
              <td className="td">is Merchant</td>
              <td className="td">Last Online At</td>
              <td className="td">is Online</td>
              <td className="td" colSpan={3}></td>
            </tr>
          </thead>
          <tbody>
            {!data ||
              (data.length === 0 && (
                <tr>
                  <td
                    className="td"
                    colSpan={9}
                    style={{ textAlign: 'center' }}
                  >
                    <p className="text_not_found">Noting Found</p>
                  </td>
                </tr>
              ))}
            {data &&
              data.length > 0 &&
              data.map((d, index) => (
                <tr key={`${d.username}-${index}`}>
                  <td className="td">
                    {d.isActivated ? <CheckIcon /> : <CloseIcon />}
                  </td>
                  <td className="td">{d.username}</td>
                  <td className="td">
                    {d.isTrader ? <CheckIcon /> : <CloseIcon />}
                  </td>
                  <td className="td">
                    {d.isMerchant ? <CheckIcon /> : <CloseIcon />}
                  </td>
                  <td className="td">{d.lastOnlineAt}</td>
                  <td className="td">
                    {d.isOnline ? <CheckIcon /> : <CloseIcon />}
                  </td>
                  <td className="td">
                    <Button
                      variant="contained"
                      href={`/users/${d.username}`}
                      startIcon={<PreviewIcon />}
                    >
                      Show
                    </Button>
                  </td>
                  <td className="td">
                    <Button
                      variant="contained"
                      href={`/users/${d.username}/edit`}
                      startIcon={<EditIcon />}
                    >
                      Edit
                    </Button>
                  </td>
                  <td className="td">
                    <Button
                      variant="contained"
                      color="error"
                      startIcon={<DeleteIcon />}
                      onClick={() => confirm(`delete user #${d.username}`)}
                    >
                      Delete
                    </Button>
                  </td>
                </tr>
              ))}
          </tbody>
        </table>
        {data && data.length > 0 && (
          <Stack spacing={2} className="pagination_container">
            <p className="pagination_container__p">Rows per page:</p>
            <select
              onChange={(e) => setItemsPerPage(Number(e.target.value))}
              style={{ marginTop: '0px', marginLeft: '16px' }}
            >
              <option value={20}>20</option>
              <option value={50}>50</option>
              <option value={100}>100</option>
            </select>
            <Pagination
              style={{ marginTop: '0px' }}
              count={pagesAll ? pagesAll : 0}
              showFirstButton
              showLastButton
              onChange={(_e, page: number) => handleChangePage(page)}
            />
          </Stack>
        )}
      </div>
    </>
  );
};

export default UsersPage;
