import { memo, useEffect, useState } from 'react';
import { Button, Pagination, Stack } from '@mui/material';
import AddIcon from '@mui/icons-material/Add';
import DeleteIcon from '@mui/icons-material/Delete';
import PreviewIcon from '@mui/icons-material/Preview';
import { useGetApiTokens } from '@app/apollo/actions';
import { ApiTokensItem, ApiTokensResponse } from '@app/interfaces';

const useInitialData = (page, itemsPerPage, pagination): ApiTokensResponse => {
  const { data } = useGetApiTokens({
    variables: {
      page,
      itemsPerPage,
      pagination,
    },
  });

  const totalItems = (data && data.apiTokens && data.apiTokens.totalItems) || 0;
  const apiTokensItems: ApiTokensItem[] =
    (data && data.apiTokens && data.apiTokens.apiTokensItems) || [];

  return {
    totalItems,
    apiTokensItems,
  };
};

const ApiTokensPage = () => {
  const [page, setPage] = useState(1);
  const [itemsPerPage, setItemsPerPage] = useState<number>(20);
  const [pagesAll, setPagesAll] = useState(0);

  const pagination = true;
  const { apiTokensItems: data, totalItems } = useInitialData(
    page,
    itemsPerPage,
    pagination,
  );

  useEffect(() => {
    const numPages = Math.ceil(totalItems / itemsPerPage);
    setPagesAll(numPages);
  }, [itemsPerPage, totalItems]);

  const handleChangePage = (page: number) => {
    setPage(page);
    const isBrowser = typeof window !== 'undefined';
    if (isBrowser) {
      window.scrollTo({ top: 0, behavior: 'smooth' });
    }
  };

  return (
    <>
      <h1>Api Tokens</h1>
      <div className="top_buttons_container">
        <Button
          href="/api_tokens/create/"
          variant="outlined"
          startIcon={<AddIcon />}
        >
          Create
        </Button>
      </div>
      <div className="table_container">
        <table className="table">
          <thead>
            <tr>
              <td className="td">Name</td>
              <td className="td">Private key</td>
              <td className="td">Is online</td>
              <td className="td">Created at</td>
              <td className="td">Last used at</td>
              <td className="td" colSpan={2}></td>
            </tr>
          </thead>
          <tbody>
            {data &&
              data.length > 0 &&
              data.map((d) => (
                <tr key={d.id}>
                  <td className="td">{d.name}</td>
                  <td className="td">{d.privateKey ? d.privateKey : ''}</td>
                  <td className="td">{d.isOnline ? 'yes' : 'no'}</td>
                  <td className="td">{d.createdAt}</td>
                  <td className="td">{d.lastUsedAt ? d.lastUsedAt : ''}</td>
                  <td className="td">
                    <Button
                      variant="contained"
                      href={`/api_tokens/${d.id}`}
                      startIcon={<PreviewIcon />}
                    >
                      Show
                    </Button>
                  </td>
                  <td className="td">
                    <Button
                      variant="contained"
                      color="error"
                      startIcon={<DeleteIcon />}
                      onClick={() => confirm(`delete api token #${d.id}`)}
                    >
                      Delete
                    </Button>
                  </td>
                </tr>
              ))}
          </tbody>
        </table>
        {data && data.length > 0 && (
          <Stack spacing={2} className="pagination_container">
            <p className="pagination_container__p">Rows per page:</p>
            <select
              onChange={(e) => setItemsPerPage(Number(e.target.value))}
              style={{ marginTop: '0px', marginLeft: '16px' }}
            >
              <option value={20}>20</option>
              <option value={50}>50</option>
              <option value={100}>100</option>
            </select>
            <Pagination
              style={{ marginTop: '0px' }}
              count={pagesAll ? pagesAll : 0}
              showFirstButton
              showLastButton
              onChange={(_e, page: number) => handleChangePage(page)}
            />
          </Stack>
        )}
      </div>
    </>
  );
};

export default memo(ApiTokensPage);
