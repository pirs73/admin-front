import { useState } from 'react';
import { useRouter } from 'next/router';
import { gql, useMutation } from '@apollo/client';
import { getErrorMessage } from '@app/lib/form';
import { AuthBox, RegisterForm } from '@app/components';
import { RegisterDataArgs } from '@app/interfaces';
import styles from './RegisterPage.module.css';

const SignUpMutation = gql`
  mutation SignUpMutation(
    $username: String!
    $password: String!
    $referralCode: String!
  ) {
    signUp(
      input: {
        username: $username
        password: $password
        referralCode: $referralCode
      }
    )
  }
`;

export const RegisterPage = () => {
  const [signUp] = useMutation(SignUpMutation);
  const [errorMsg, setErrorMsg] = useState();
  const [isLoading, setoIsLoading] = useState(false);
  const router = useRouter();

  async function handleSubmit(signUpData: any) {
    const { username, password, referralCode } = signUpData;

    try {
      const { data, loading, error } = await signUp({
        variables: {
          username,
          password,
          referralCode,
        },
      });

      if (loading) {
        setoIsLoading(true);
      } else {
        setoIsLoading(false);
      }

      if (error) {
        console.error(error);
      }

      if (data?.signUp) {
        router.push('/login');
      }
    } catch (error) {
      setErrorMsg(getErrorMessage(error));
    }
  }

  return (
    <AuthBox isRegisterForm>
      <h2 className={styles.title}>Create an account</h2>
      <RegisterForm
        loading={isLoading}
        handleRegisterSubmit={(signUpData: RegisterDataArgs) => {
          handleSubmit(signUpData);
        }}
      />
      {errorMsg && <p className={styles.warning}>{errorMsg}</p>}
    </AuthBox>
  );
};
