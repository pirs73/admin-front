import { useEffect, useState } from 'react';
import Link from 'next/link';
import { Button, Pagination, Stack } from '@mui/material';
import AddIcon from '@mui/icons-material/Add';
import EditIcon from '@mui/icons-material/Edit';
import PreviewIcon from '@mui/icons-material/Preview';
import CheckIcon from '@mui/icons-material/Check';
import CloseIcon from '@mui/icons-material/Close';
import { useGetTrades } from '@app/apollo/actions';
import { TradesItem, TradesResponse } from '@app/interfaces';

const useInitialData = (page, itemsPerPage, pagination): TradesResponse => {
  const { data } = useGetTrades({
    variables: {
      page,
      itemsPerPage,
      pagination,
    },
  });
  const totalItems = (data && data.trades && data.trades.totalItems) || 0;
  const tradesItems: TradesItem[] =
    (data && data.trades && data.trades.tradesItems) || [];

  return {
    totalItems,
    tradesItems,
  };
};

const TradesPage = () => {
  const [page, setPage] = useState(1);
  const [itemsPerPage, setItemsPerPage] = useState<number>(20);
  const [pagesAll, setPagesAll] = useState(0);

  const { tradesItems: data, totalItems } = useInitialData(
    page,
    itemsPerPage,
    true,
  );

  useEffect(() => {
    const numPages = Math.ceil(totalItems / itemsPerPage);
    setPagesAll(numPages);
  }, [itemsPerPage, totalItems]);

  const handleChangePage = (page: number) => {
    setPage(page);
    const isBrowser = typeof window !== 'undefined';
    if (isBrowser) {
      window.scrollTo({ top: 0, behavior: 'smooth' });
    }
  };

  return (
    <>
      <h1>Trades</h1>
      <div className="top_buttons_container">
        <Button
          href="/trades/create/"
          variant="outlined"
          startIcon={<AddIcon />}
        >
          Create
        </Button>
      </div>
      <div className="table_container">
        <table className="table">
          <thead>
            <tr>
              <td className="td">Currency Pair</td>
              <td className="td">Amount</td>
              <td className="td">Rate</td>
              <td className="td">Created At</td>
              <td className="td">Completed At</td>
              <td className="td">User</td>
              <td className="td">Ad Detail</td>
              <td className="td">Is Confirmed</td>
              <td className="td">Is On Dispute</td>
              <td className="td">Is Payment Send</td>
              <td className="td">Is Canceled</td>
              <td className="td" colSpan={2}></td>
            </tr>
          </thead>
          <tbody>
            {!data ||
              (data.length === 0 && (
                <tr>
                  <td
                    className="td"
                    colSpan={13}
                    style={{ textAlign: 'center' }}
                  >
                    <p className="text_not_found">Noting Found</p>
                  </td>
                </tr>
              ))}
            {data &&
              data.length > 0 &&
              data.map((d, index) => (
                <tr key={`${d.uuid}-${index}`}>
                  <td className="td">
                    <Link href={`/currency_pairs/${d.currencyPair}`}>
                      {d.currencyPair}
                    </Link>
                  </td>
                  <td className="td">{d.amount}</td>
                  <td className="td">{d.rate}</td>
                  <td className="td">{d.createdAt}</td>
                  <td className="td">{d.completedAt}</td>
                  <td className="td">{d.user}</td>
                  <td className="td">{d.adDetail}</td>
                  <td className="td">
                    {d.isConfirmed ? <CheckIcon /> : <CloseIcon />}
                  </td>
                  <td className="td">
                    {d.isOnDispute ? <CheckIcon /> : <CloseIcon />}
                  </td>
                  <td className="td">
                    {d.isPaymentSend ? <CheckIcon /> : <CloseIcon />}
                  </td>
                  <td className="td">
                    {d.isCanceled ? <CheckIcon /> : <CloseIcon />}
                  </td>
                  <td className="td">
                    {d.uuid && (
                      <Button
                        variant="contained"
                        href={`/trades/${d.uuid}`}
                        startIcon={<PreviewIcon />}
                      >
                        Show
                      </Button>
                    )}
                  </td>
                  <td className="td">
                    {d.uuid !== '' && (
                      <Button
                        variant="contained"
                        color="warning"
                        href={`/trades/${d.uuid}/edit`}
                        startIcon={<EditIcon />}
                      >
                        Edit
                      </Button>
                    )}
                  </td>
                </tr>
              ))}
          </tbody>
        </table>
        {data && data.length > 0 && (
          <Stack spacing={2} className="pagination_container">
            <p className="pagination_container__p">Rows per page:</p>
            <select
              onChange={(e) => setItemsPerPage(Number(e.target.value))}
              style={{ marginTop: '0px', marginLeft: '16px' }}
            >
              <option value={20}>20</option>
              <option value={50}>50</option>
              <option value={100}>100</option>
            </select>
            <Pagination
              style={{ marginTop: '0px' }}
              count={pagesAll ? pagesAll : 0}
              showFirstButton
              showLastButton
              onChange={(_e, page: number) => handleChangePage(page)}
            />
          </Stack>
        )}
      </div>
    </>
  );
};

export default TradesPage;
