import Stack from '@mui/material/Stack';
import CheckIcon from '@mui/icons-material/Check';
import CloseIcon from '@mui/icons-material/Close';
import { useGetAdResource } from '@app/apollo/actions';
import { AdsItemPageProps } from './AdsItemPage.props';
import styles from './AdsItemPage.module.css';

const useInitialData = (uuid: string) => {
  const { data } = useGetAdResource({
    variables: {
      uuid,
    },
  });

  const adsItem = (data && data.getAdResource) || null;

  return {
    adsItem,
  };
};

const AdsItemPage = ({ uuid }: AdsItemPageProps) => {
  const { adsItem: data } = useInitialData(uuid);
  return (
    <>
      {data && (
        <Stack spacing={2}>
          <p>UUID: {uuid}</p>
          <p>Bank: {data.bank?.name}</p>
          <p>Currency Pair: {data.currencyPair?.name}</p>
          <p>Details</p>
          <ul>
            {data.details &&
              data.details.length > 0 &&
              data.details.map((d, index) => (
                <li key={`${index}-${d.uuid}`}>
                  <span className={styles.detail_text}>
                    Name: {d.text && d.text}
                  </span>
                  <span className={styles.detail_text}>Number: {d.number}</span>
                  <span className={styles.detail_text}>
                    transactionSum: {d.transactionSum}
                  </span>
                  <span className={styles.detail_text}>
                    UUID: {d.uuid && d.uuid}
                  </span>
                </li>
              ))}
          </ul>
          <p>isActive: {data.isActive ? <CheckIcon /> : <CloseIcon />}</p>
          <p>maxAmount: {data.maxAmount}</p>
          <p>minAmount: {data.minAmount}</p>
          <p>minAmount: {data.minAmount}</p>
          <p>User: {data.user && data.user}</p>
        </Stack>
      )}
    </>
  );
};

export default AdsItemPage;
