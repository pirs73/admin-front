import { DetailedHTMLProps, HTMLAttributes } from 'react';

export interface AdsItemPageProps
  extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
  uuid: string;
}
