import styles from './LoginPageHeader.module.css';

const LoginPageHeader = () => {
  return (
    <>
      <h2 className={styles.title}>Welcome Back!</h2>
      <p className={styles.text}>We are happy that you are with us!</p>
    </>
  );
};

export { LoginPageHeader };
