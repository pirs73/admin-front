import { useState } from 'react';
import { useRouter } from 'next/navigation';
import { useApolloClient } from '@apollo/client';
import { useSignIn } from '@app/apollo/actions';
import { getErrorMessage } from '@app/lib/form';
import { AuthBox, LoginForm } from '@app/components';
import { LoginPageHeader } from './';
import { LoginDataArgs } from '@app/interfaces';
import styles from './LoginPage.module.css';

export const LoginPage = () => {
  const [signIn] = useSignIn();
  const client = useApolloClient();
  const [errorMsg, setErrorMsg] = useState();
  const [isLoading, setoIsLoading] = useState(false);
  const router = useRouter();

  async function handleSubmit(signInData) {
    const { username, password } = signInData;

    try {
      await client.resetStore();
      const { data, loading, error } = await signIn({
        variables: {
          username,
          password,
        },
      });

      if (loading) {
        setoIsLoading(true);
      } else {
        setoIsLoading(false);
      }

      if (error) {
        console.error(error);
      }

      if (data?.signIn) {
        router.push('/ads');
      }
    } catch (error) {
      console.error(error);
      setErrorMsg(getErrorMessage(error));
    }
  }

  return (
    <AuthBox>
      <LoginPageHeader />
      <LoginForm
        loading={isLoading}
        handleLoginSubmit={(signInData: LoginDataArgs) =>
          handleSubmit(signInData)
        }
      />
      {errorMsg && <p className={styles.warning}>{errorMsg}</p>}
    </AuthBox>
  );
};
