import { memo, useEffect, useState } from 'react';
import Link from 'next/link';
import { Button, Pagination, Stack } from '@mui/material';
import AddIcon from '@mui/icons-material/Add';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import PreviewIcon from '@mui/icons-material/Preview';
import { useGetCashbacks } from '@app/apollo/actions';
import { CashbacksItem, CashbacksResponse } from '@app/interfaces';

const useInitialData = (page, itemsPerPage, pagination): CashbacksResponse => {
  const { data } = useGetCashbacks({
    variables: {
      page,
      itemsPerPage,
      pagination,
    },
  });

  const totalItems = (data && data.cashbacks && data.cashbacks.totalItems) || 0;
  const cashbacksItems: CashbacksItem[] =
    (data && data.cashbacks && data.cashbacks.cashbacksItems) || [];

  return {
    totalItems,
    cashbacksItems,
  };
};

const CashbacksPage = () => {
  const [page, setPage] = useState(1);
  const [itemsPerPage, setItemsPerPage] = useState<number>(20);
  const [pagesAll, setPagesAll] = useState(0);

  const { cashbacksItems: data, totalItems } = useInitialData(
    page,
    itemsPerPage,
    true,
  );

  useEffect(() => {
    const numPages = Math.ceil(totalItems / itemsPerPage);
    setPagesAll(numPages);
  }, [itemsPerPage, totalItems]);

  const handleChangePage = (page: number) => {
    setPage(page);
    const isBrowser = typeof window !== 'undefined';
    if (isBrowser) {
      window.scrollTo({ top: 0, behavior: 'smooth' });
    }
  };

  return (
    <>
      <h1>Cashbacks</h1>
      <div className="top_buttons_container">
        <Button
          href="/cashbacks/create/"
          variant="outlined"
          startIcon={<AddIcon />}
        >
          Create
        </Button>
      </div>
      <div className="table_container">
        <table className="table">
          <thead>
            <tr>
              <td className="td">Cashback to user</td>
              <td className="td">Cashback from merchant</td>
              <td className="td">Cashback from user</td>
              <td className="td">Cashback sell crypto</td>
              <td className="td">Cashback buy crypto</td>
              <td className="td" colSpan={3}></td>
            </tr>
          </thead>
          <tbody>
            {data &&
              data.length > 0 &&
              data.map((d) => (
                <tr key={d.id}>
                  <td className="td">
                    <Link href={`/users/${d.cashbackToUser}`}>
                      {d.cashbackToUser}
                    </Link>
                  </td>
                  <td className="td">
                    {d.cashbackFromMerchant ? (
                      <Link href={`/merchants/${d.cashbackFromMerchant}`}>
                        {d.cashbackFromMerchant}
                      </Link>
                    ) : (
                      ''
                    )}
                  </td>
                  <td className="td">
                    {d.cashbackFromUser ? (
                      <Link href={`/users/${d.cashbackFromUser}`}>
                        {d.cashbackFromUser}
                      </Link>
                    ) : (
                      ''
                    )}
                  </td>
                  <td className="td">{d.cashbackSellCrypto}</td>
                  <td className="td">{d.cashbackBuyCrypto}</td>
                  <td className="td">
                    <Button
                      variant="contained"
                      href={`/cashbacks/${d.id}`}
                      startIcon={<PreviewIcon />}
                    >
                      Show
                    </Button>
                  </td>
                  <td className="td">
                    <Button
                      variant="contained"
                      color="warning"
                      href={`/cashbacks/${d.id}/edit`}
                      startIcon={<EditIcon />}
                    >
                      Edit
                    </Button>
                  </td>
                  <td className="td">
                    <Button
                      variant="contained"
                      color="error"
                      startIcon={<DeleteIcon />}
                      onClick={() => confirm(`delete ad #${d.id}`)}
                    >
                      Delete
                    </Button>
                  </td>
                </tr>
              ))}
          </tbody>
        </table>
        {data && data.length > 0 && (
          <Stack spacing={2} className="pagination_container">
            <p className="pagination_container__p">Rows per page:</p>
            <select
              onChange={(e) => setItemsPerPage(Number(e.target.value))}
              style={{ marginTop: '0px', marginLeft: '16px' }}
            >
              <option value={20}>20</option>
              <option value={50}>50</option>
              <option value={100}>100</option>
            </select>
            <Pagination
              style={{ marginTop: '0px' }}
              count={pagesAll ? pagesAll : 0}
              showFirstButton
              showLastButton
              onChange={(_e, page: number) => handleChangePage(page)}
            />
          </Stack>
        )}
      </div>
    </>
  );
};

export default memo(CashbacksPage);
