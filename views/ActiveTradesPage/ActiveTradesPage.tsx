// import { useEffect, useState } from 'react';
// import Link from 'next/link';
// import { Button, Pagination, Stack } from '@mui/material';
// import AddIcon from '@mui/icons-material/Add';
// import EditIcon from '@mui/icons-material/Edit';
// import PreviewIcon from '@mui/icons-material/Preview';
// import CheckIcon from '@mui/icons-material/Check';
// import CloseIcon from '@mui/icons-material/Close';
// import { useGetTrades } from '@app/apollo/actions';
// import { TradesItem, TradesResponse } from '@app/interfaces';

// const useInitialData = (
//   page,
//   itemsPerPage,
//   pagination,
//   isPaymentSend,
//   isOnDispute,
//   isConfirmed,
// ): TradesResponse => {
//   const { data } = useGetTrades({
//     variables: {
//       page,
//       itemsPerPage,
//       pagination,
//       isPaymentSend,
//       isOnDispute,
//       isConfirmed,
//     },
//   });

//   const totalItems = (data && data.trades && data.trades.totalItems) || 0;
//   const tradesItems: TradesItem[] =
//     (data && data.trades && data.trades.tradesItems) || [];

//   return {
//     totalItems,
//     tradesItems,
//   };
// };

// const TradesPage = () => {
//   const [pageOnDispute, setPageOnDispute] = useState(1);
//   const [itemsPerPageOnDispute, setItemsPerPageOnDispute] =
//     useState<number>(20);
//   const [pagesAllOnDispute, setPagesAllOnDispute] = useState(0);

//   const { tradesItems: dataOnDispute, totalItems: totalItemsOnDispute } =
//     useInitialData(
//       pageOnDispute,
//       itemsPerPageOnDispute,
//       true,
//       false,
//       true,
//       false,
//     );

//   useEffect(() => {
//     const numPagesOnDispute = Math.ceil(
//       totalItemsOnDispute / itemsPerPageOnDispute,
//     );
//     setPagesAllOnDispute(numPagesOnDispute);
//   }, [itemsPerPageOnDispute, totalItemsOnDispute]);

//   return (
//     <>
//       <h1>Trades</h1>
//       <h2>OnDispute</h2>
//       <div className="table_container">
//         <table className="table">
//           <thead>
//             <tr>
//               <td>Trade</td>
//               <td>Currency Pair</td>
//               <td>Amount</td>
//               <td>Rate</td>
//               <td>Created At</td>
//               <td>Completed At</td>
//               <td>User</td>
//               <td>Ad Detail</td>
//               <td>Is Confirmed</td>
//               <td>Is On Dispute</td>
//               <td>Is Payment Send</td>
//               <td>Is Canceled</td>
//               <td>show</td>
//               <td>edit</td>
//             </tr>
//           </thead>
//           <tbody>
//             {data &&
//               data.length > 0 &&
//               data.map((d, index) => (
//                 <tr key={`${d.uuid}-${index}`}>
//                   <td>{d.currencyPair}</td>
//                   <td>{d.amount}</td>
//                   <td>{d.rate}</td>
//                   <td>{d.createdAt}</td>
//                   <td>{d.completedAt}</td>
//                   <td>{d.user}</td>
//                   <td>{d.adDetail}</td>
//                   <td>{d.isConfirmed ? 'Yes' : 'No'}</td>
//                   <td>{d.isOnDispute ? 'Yes' : 'No'}</td>
//                   <td>{d.isPaymentSend ? 'Yes' : 'No'}</td>
//                   <td>{d.isCanceled ? 'Yes' : 'No'}</td>
//                   <td>{d.uuid !== '' && <button>show</button>}</td>
//                   <td>{d.uuid !== '' && <button>edit</button>}</td>
//                 </tr>
//               ))}
//           </tbody>
//         </table>
//       </div>
//     </>
//   );
// };

// export default TradesPage;
