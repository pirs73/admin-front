import { memo, useEffect, useState } from 'react';
import { Button, Pagination, Stack } from '@mui/material';
import PreviewIcon from '@mui/icons-material/Preview';
import CheckIcon from '@mui/icons-material/Check';
import CloseIcon from '@mui/icons-material/Close';
import { useGetCurrencies } from '@app/apollo/actions';
import { CurrenciesItem, CurrenciesResponse } from '@app/interfaces';

const useInitialData = (page, itemsPerPage, pagination): CurrenciesResponse => {
  const { data } = useGetCurrencies({
    variables: {
      page,
      itemsPerPage,
      pagination,
    },
  });
  const totalItems =
    (data && data.currencies && data.currencies.totalItems) || 0;
  const currenciesItems: CurrenciesItem[] =
    (data && data.currencies && data.currencies.currenciesItems) || [];

  return {
    totalItems,
    currenciesItems,
  };
};

const CurrenciesPage = () => {
  const [page, setPage] = useState(1);
  const [itemsPerPage, setItemsPerPage] = useState<number>(20);
  const [pagesAll, setPagesAll] = useState(0);
  const { currenciesItems: data, totalItems } = useInitialData(
    page,
    itemsPerPage,
    true,
  );

  useEffect(() => {
    const numPages = Math.ceil(totalItems / itemsPerPage);
    setPagesAll(numPages);
  }, [itemsPerPage, totalItems]);

  const handleChangePage = (page: number) => {
    setPage(page);
    const isBrowser = typeof window !== 'undefined';
    if (isBrowser) {
      window.scrollTo({ top: 0, behavior: 'smooth' });
    }
  };

  return (
    <>
      <h1>Currencies</h1>
      <div className="table_container">
        <table className="table">
          <thead>
            <tr>
              <td className="td">Name</td>
              <td className="td">Symbol</td>
              <td className="td">Icon</td>
              <td className="td">isFiat</td>
              <td className="td"></td>
            </tr>
          </thead>
          <tbody>
            {data &&
              data.length > 0 &&
              data.map((d) => (
                <tr key={d.id}>
                  <td className="td">{d.name}</td>
                  <td className="td">{d.symbol}</td>
                  <td className="td">{d.icon ? d.icon : ''}</td>
                  <td className="td">
                    {d.isFiat ? <CheckIcon /> : <CloseIcon />}
                  </td>
                  <td className="td">
                    <Button
                      variant="contained"
                      href={`/currencies/${d.id}`}
                      startIcon={<PreviewIcon />}
                    >
                      Show
                    </Button>
                  </td>
                </tr>
              ))}
          </tbody>
        </table>
        {data && data.length > 0 && (
          <Stack spacing={2} className="pagination_container">
            <p className="pagination_container__p">Rows per page:</p>
            <select
              onChange={(e) => setItemsPerPage(Number(e.target.value))}
              style={{ marginTop: '0px', marginLeft: '16px' }}
            >
              <option value={20}>20</option>
              <option value={50}>50</option>
              <option value={100}>100</option>
            </select>
            <Pagination
              style={{ marginTop: '0px' }}
              count={pagesAll ? pagesAll : 0}
              showFirstButton
              showLastButton
              onChange={(_e, page: number) => handleChangePage(page)}
            />
          </Stack>
        )}
      </div>
    </>
  );
};

export default memo(CurrenciesPage);
