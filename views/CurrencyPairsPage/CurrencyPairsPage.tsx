import { memo, useEffect, useState } from 'react';
import { Button, Pagination, Stack } from '@mui/material';
import PreviewIcon from '@mui/icons-material/Preview';
import { useGetCurrencyPairs } from '@app/apollo/actions';
import { CurrencyPairsItem, CurrencyPairsResponse } from '@app/interfaces';

const useInitialData = (
  page,
  itemsPerPage,
  pagination,
): CurrencyPairsResponse => {
  const { data } = useGetCurrencyPairs({
    variables: {
      page,
      itemsPerPage,
      pagination,
    },
  });

  const totalItems =
    (data && data.currencyPairs && data.currencyPairs.totalItems) || 0;
  const currencyPairsItems: CurrencyPairsItem[] =
    (data && data.currencyPairs && data.currencyPairs.currencyPairsItems) || [];

  return {
    totalItems,
    currencyPairsItems,
  };
};

const CurrencyPairsPage = () => {
  const [page, setPage] = useState(1);
  const [itemsPerPage, setItemsPerPage] = useState<number>(20);
  const [pagesAll, setPagesAll] = useState(0);

  const { currencyPairsItems: data, totalItems } = useInitialData(
    page,
    itemsPerPage,
    true,
  );

  useEffect(() => {
    const numPages = Math.ceil(totalItems / itemsPerPage);
    setPagesAll(numPages);
  }, [itemsPerPage, totalItems]);

  const handleChangePage = (page: number) => {
    setPage(page);
    const isBrowser = typeof window !== 'undefined';
    if (isBrowser) {
      window.scrollTo({ top: 0, behavior: 'smooth' });
    }
  };

  return (
    <>
      <h1>Currency Pairs</h1>
      <div className="table_container">
        <table className="table">
          <thead>
            <tr>
              <td className="td">Currency From</td>
              <td className="td">Currency To</td>
              <td className="td">Exchange Rate</td>
              <td className="td"></td>
            </tr>
          </thead>
          <tbody>
            {!data ||
              (data.length === 0 && (
                <tr>
                  <td
                    className="td"
                    colSpan={4}
                    style={{ textAlign: 'center' }}
                  >
                    <p className="text_not_found">Noting Found</p>
                  </td>
                </tr>
              ))}
            {data &&
              data.length > 0 &&
              data.map((d) => (
                <tr key={d.name}>
                  <td className="td">{d.currencyFrom}</td>
                  <td className="td">{d.currencyTo}</td>
                  <td className="td">{d.exchangeRate ? d.exchangeRate : ''}</td>
                  <td className="td">
                    <Button
                      variant="contained"
                      href={`/currency_pairs/${d.name}`}
                      startIcon={<PreviewIcon />}
                    >
                      Show
                    </Button>
                  </td>
                </tr>
              ))}
          </tbody>
        </table>
        {data && data.length > 0 && (
          <Stack spacing={2} className="pagination_container">
            <p className="pagination_container__p">Rows per page:</p>
            <select
              onChange={(e) => setItemsPerPage(Number(e.target.value))}
              style={{ marginTop: '0px', marginLeft: '16px' }}
            >
              <option value={20}>20</option>
              <option value={50}>50</option>
              <option value={100}>100</option>
            </select>
            <Pagination
              style={{ marginTop: '0px' }}
              count={pagesAll ? pagesAll : 0}
              showFirstButton
              showLastButton
              onChange={(_e, page: number) => handleChangePage(page)}
            />
          </Stack>
        )}
      </div>
    </>
  );
};

export default memo(CurrencyPairsPage);
