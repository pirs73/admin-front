import { useEffect, useState } from 'react';
import cn from 'classnames';
import Pagination from '@mui/material/Pagination';
import Stack from '@mui/material/Stack';
import { useGetLogs } from '@app/apollo/actions';
import { LogsItem, LogsResponse } from '@app/interfaces';
import styles from './LogsPage.module.css';

const useInitialData = (page, itemsPerPage, pagination): LogsResponse => {
  const { data } = useGetLogs({
    variables: {
      page,
      itemsPerPage,
      pagination,
    },
  });

  const totalItems = (data && data.logs && data.logs.totalItems) || 0;
  const logsItems: LogsItem[] =
    (data && data?.logs && data?.logs?.logsItems) || [];

  return {
    totalItems,
    logsItems,
  };
};

const LogsPage = () => {
  const [page, setPage] = useState(1);
  const [itemsPerPage, setItemsPerPage] = useState<number>(20);
  const [pagesAll, setPagesAll] = useState(0);

  const pagination = true;
  const { logsItems: data, totalItems } = useInitialData(
    page,
    itemsPerPage,
    pagination,
  );

  useEffect(() => {
    const numPages = Math.ceil(totalItems / itemsPerPage);
    setPagesAll(numPages);
  }, [itemsPerPage, totalItems]);

  const handleChangePage = (page: number) => {
    setPage(page);
    const isBrowser = typeof window !== 'undefined';
    if (isBrowser) {
      window.scrollTo({ top: 0, behavior: 'smooth' });
    }
  };

  return (
    <>
      <h1>Logs</h1>
      <div className="table_container">
        <table className="table">
          <thead>
            <tr>
              <td className={cn('td')}>Channel</td>
              <td className={cn('td')}>Level</td>
              <td className={cn('td')}>Message</td>
              <td className={cn('td', styles.td_context)}>Context</td>
              <td className={cn('td')}>belongToUser</td>
              <td className={cn('td')}>extra</td>
              <td className={cn('td')}>createdAt</td>
            </tr>
          </thead>
          <tbody>
            {data &&
              data.length > 0 &&
              data.map((d) => (
                <tr key={d.id}>
                  <td className={cn('td')}>{d.channel}</td>
                  <td className={cn('td')}>{d.level}</td>
                  <td className={cn('td')}>{d.message}</td>
                  <td className={cn('td', styles.td_context)}>
                    {JSON.stringify(d.context).toString()}
                  </td>
                  <td className={cn('td')}>{d.belongToUser}</td>
                  <td className={cn('td')}>{d.extra}</td>
                  <td className={cn('td')}>{d.createdAt}</td>
                </tr>
              ))}
          </tbody>
        </table>
        {data && data.length > 0 && (
          <Stack spacing={2} className="pagination_container">
            <p className="pagination_container__p">Rows per page:</p>
            <select
              onChange={(e) => setItemsPerPage(Number(e.target.value))}
              style={{ marginTop: '0px', marginLeft: '16px' }}
            >
              <option value={20}>20</option>
              <option value={50}>50</option>
              <option value={100}>100</option>
            </select>
            <Pagination
              style={{ marginTop: '0px' }}
              count={pagesAll ? pagesAll : 0}
              showFirstButton
              showLastButton
              onChange={(_e, page: number) => handleChangePage(page)}
            />
          </Stack>
        )}
      </div>
    </>
  );
};

export default LogsPage;
