import { memo, useEffect, useState } from 'react';
import { Button, Pagination, Stack } from '@mui/material';
import AddIcon from '@mui/icons-material/Add';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import PreviewIcon from '@mui/icons-material/Preview';
import CheckIcon from '@mui/icons-material/Check';
import CloseIcon from '@mui/icons-material/Close';
import { useGetMerchants } from '@app/apollo/actions';
import { MerchantsItem, MerchantsResponse } from '@app/interfaces';

const useInitialData = (page, itemsPerPage, pagination): MerchantsResponse => {
  const { data } = useGetMerchants({
    variables: {
      page,
      itemsPerPage,
      pagination,
    },
  });

  const totalItems = (data && data.merchants && data.merchants.totalItems) || 0;
  const merchantsItems: MerchantsItem[] =
    (data && data.merchants && data.merchants.merchantsItems) || [];

  return {
    totalItems,
    merchantsItems,
  };
};

const MerchantsPage = () => {
  const [page, setPage] = useState(1);
  const [itemsPerPage, setItemsPerPage] = useState<number>(20);
  const [pagesAll, setPagesAll] = useState(0);

  const { merchantsItems: data, totalItems } = useInitialData(
    page,
    itemsPerPage,
    true,
  );

  useEffect(() => {
    const numPages = Math.ceil(totalItems / itemsPerPage);
    setPagesAll(numPages);
  }, [itemsPerPage, totalItems]);

  const handleChangePage = (page: number) => {
    setPage(page);
    const isBrowser = typeof window !== 'undefined';
    if (isBrowser) {
      window.scrollTo({ top: 0, behavior: 'smooth' });
    }
  };

  return (
    <>
      <h1>Merchants</h1>
      <div className="top_buttons_container">
        <Button
          href="/merchants/create/"
          variant="outlined"
          startIcon={<AddIcon />}
        >
          Create
        </Button>
      </div>
      <div className="table_container">
        <table className="table">
          <thead>
            <tr>
              <td className="td">Name</td>
              <td className="td">User</td>
              <td className="td">Discount Sell Crypto</td>
              <td className="td">Discount Buy Crypto</td>
              <td className="td">Is Allowed</td>
              <td className="td">Is Active</td>
              <td className="td">Created At</td>
              <td colSpan={3} className="td"></td>
            </tr>
          </thead>
          <tbody>
            {!data ||
              (data.length === 0 && (
                <tr>
                  <td
                    className="td"
                    colSpan={10}
                    style={{ textAlign: 'center' }}
                  >
                    <p className="text_not_found">Noting Found</p>
                  </td>
                </tr>
              ))}
            {data &&
              data.length > 0 &&
              data.map((d) => (
                <tr key={`${d.name}-${d.uuid}`}>
                  <td className="td">{d.name}</td>
                  <td className="td">{d.user}</td>
                  <td className="td">{d.discountSellCrypto}</td>
                  <td className="td">{d.discountBuyCrypto}</td>
                  <td className="td">
                    {d.isAllowed ? <CheckIcon /> : <CloseIcon />}
                  </td>
                  <td className="td">
                    {d.isActive ? <CheckIcon /> : <CloseIcon />}
                  </td>
                  <td className="td">{d.createdAt}</td>
                  <td className="td">
                    {d.uuid && (
                      <Button
                        variant="contained"
                        href={`/merchants/${d.uuid}`}
                        startIcon={<PreviewIcon />}
                      >
                        Show
                      </Button>
                    )}
                  </td>
                  <td className="td">
                    {d.uuid && (
                      <Button
                        variant="contained"
                        color="warning"
                        href={`/merchants/${d.uuid}/edit`}
                        startIcon={<EditIcon />}
                      >
                        Edit
                      </Button>
                    )}
                  </td>
                  <td className="td">
                    {d.uuid && (
                      <Button
                        variant="contained"
                        color="error"
                        startIcon={<DeleteIcon />}
                        onClick={() => confirm(`delete merchant #${d.uuid}`)}
                      >
                        Delete
                      </Button>
                    )}
                  </td>
                </tr>
              ))}
          </tbody>
        </table>
        {data && data.length > 0 && (
          <Stack spacing={2} className="pagination_container">
            <p className="pagination_container__p">Rows per page:</p>
            <select
              onChange={(e) => setItemsPerPage(Number(e.target.value))}
              style={{ marginTop: '0px', marginLeft: '16px' }}
            >
              <option value={20}>20</option>
              <option value={50}>50</option>
              <option value={100}>100</option>
            </select>
            <Pagination
              style={{ marginTop: '0px' }}
              count={pagesAll ? pagesAll : 0}
              showFirstButton
              showLastButton
              onChange={(_e, page: number) => handleChangePage(page)}
            />
          </Stack>
        )}
      </div>
    </>
  );
};

export default memo(MerchantsPage);
