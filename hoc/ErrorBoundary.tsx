import React, { Component, ReactNode } from 'react';

interface errorBoundaryProps {
  errorUI?: ReactNode;
  children: ReactNode;
}

interface errorBoundaryState {
  hasError: boolean;
  message: string;
}

class ErrorBoundary extends Component<errorBoundaryProps, errorBoundaryState> {
  constructor(props: errorBoundaryProps) {
    super(props);
    this.state = { hasError: false, message: 'Oops, there is an error!' };
  }

  static getDerivedStateFromError(error: any) {
    return {
      hasError: true,
      message: error,
    };
  }

  componentDidCatch(error: Error, errorInfo: React.ErrorInfo): void {
    console.error({ error, errorInfo });
  }

  render() {
    if (this.state.hasError) {
      if (this.props.errorUI) {
        return this.props.errorUI;
      } else {
        return (
          <div>
            <h2>{this.state.message}</h2>
            <button
              type="button"
              onClick={() => this.setState({ hasError: false })}
            >
              Try again?
            </button>
          </div>
        );
      }
    } else {
      return this.props.children;
    }
  }
}

export default ErrorBoundary;
