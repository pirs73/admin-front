FROM node:18-alpine
WORKDIR /opt/app
ADD socket/package.json package.json
RUN npm install
COPY socket/ ./
CMD ["npm", "start"]
EXPOSE 5002
