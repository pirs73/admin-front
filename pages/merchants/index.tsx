import { BaseLayout, PageLayout } from '@app/layouts';
import { MerchantsPage } from '@app/views';

const Merchants = () => {
  return (
    <BaseLayout title="Merchants">
      <PageLayout>
        <MerchantsPage />
      </PageLayout>
    </BaseLayout>
  );
};

export default Merchants;
