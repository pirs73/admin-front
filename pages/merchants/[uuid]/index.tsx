import { BaseLayout, PageLayout } from '@app/layouts';

export async function getServerSideProps({ _req, query }) {
  return { props: { query } };
}

const Merchant = ({ query }) => {
  const { uuid } = query;

  return (
    <BaseLayout title={`Merchant: ${uuid}`}>
      <PageLayout>
        <div>
          <h1>Merchant # {uuid}</h1>
        </div>
      </PageLayout>
    </BaseLayout>
  );
};

export default Merchant;
