import { BaseLayout, PageLayout } from '@app/layouts';

export async function getServerSideProps({ _req, query }) {
  return { props: { query } };
}

const EditMerchant = ({ query }) => {
  const { uuid } = query;

  return (
    <BaseLayout title={`Edit Merchant: ${uuid}`}>
      <PageLayout>
        <div>
          <h1>Edit Merchant # {uuid}</h1>
        </div>
      </PageLayout>
    </BaseLayout>
  );
};

export default EditMerchant;
