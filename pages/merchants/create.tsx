import { BaseLayout, PageLayout } from '@app/layouts';

const CreateMerchant = () => {
  return (
    <BaseLayout title="Create Merchant">
      <PageLayout>
        <div>
          <h1>Create Merchant</h1>
        </div>
      </PageLayout>
    </BaseLayout>
  );
};

export default CreateMerchant;
