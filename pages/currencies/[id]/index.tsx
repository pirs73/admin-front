import { BaseLayout, PageLayout } from '@app/layouts';

export async function getServerSideProps({ _req, query }) {
  return { props: { query } };
}

const Currency = ({ query }) => {
  const { id } = query;

  return (
    <BaseLayout title={`Currency: ${id}`}>
      <PageLayout>
        <div>
          <h1>Currency: {id}</h1>
        </div>
      </PageLayout>
    </BaseLayout>
  );
};

export default Currency;
