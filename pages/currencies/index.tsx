import { BaseLayout, PageLayout } from '@app/layouts';
import { CurrenciesPage } from '@app/views';

const Currencies = () => {
  return (
    <BaseLayout title="Currencies">
      <PageLayout>
        <CurrenciesPage />
      </PageLayout>
    </BaseLayout>
  );
};

export default Currencies;
