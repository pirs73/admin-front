import { BaseLayout, PageLayout } from '@app/layouts';

export async function getServerSideProps({ _req, query }) {
  return { props: { query } };
}

const EditAdsItem = ({ query }) => {
  const { uuid } = query;

  return (
    <BaseLayout title={`Edit Ad: ${uuid}`}>
      <PageLayout>
        <div>
          <h1>Edit Ad # {uuid}</h1>
        </div>
      </PageLayout>
    </BaseLayout>
  );
};

export default EditAdsItem;
