import { BaseLayout, PageLayout } from '@app/layouts';
import { AdsItemPage } from '@app/views';

export function getServerSideProps({ _req, query }) {
  return { props: { query } };
}

const AdsItem = ({ query }) => {
  const { uuid } = query;

  return (
    <BaseLayout title={`Ad: ${uuid}`}>
      <PageLayout>
        <AdsItemPage uuid={uuid} />
      </PageLayout>
    </BaseLayout>
  );
};

export default AdsItem;
