import { BaseLayout, PageLayout } from '@app/layouts';
import { AdsPage } from '@app/views';

const Ads = () => {
  return (
    <BaseLayout title="Ads">
      <PageLayout>
        <AdsPage />
      </PageLayout>
    </BaseLayout>
  );
};

export default Ads;
