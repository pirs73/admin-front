import { BaseLayout, PageLayout } from '@app/layouts';

const CreateAdsItem = () => {
  return (
    <BaseLayout title="Create Ad">
      <PageLayout>
        <div>
          <h1>Create Ad</h1>
        </div>
      </PageLayout>
    </BaseLayout>
  );
};

export default CreateAdsItem;
