import { AppProps } from 'next/app';
import { CacheProvider, EmotionCache } from '@emotion/react';
import ErrorBoundary from '@app/hoc/ErrorBoundary';
import createEmotionCache from '@app/lib/createEmotionCache';
import '@app/assets/css/styles.css';
import withApollo from '@app/hoc/withApollo';

// Client-side cache, shared for the whole session of the user in the browser.
const clientSideEmotionCache = createEmotionCache();

export interface MyAppProps extends AppProps {
  emotionCache?: EmotionCache;
}

const MyApp = ({
  Component,
  emotionCache = clientSideEmotionCache,
  pageProps,
}: MyAppProps) => {
  return (
    <ErrorBoundary>
      <CacheProvider value={emotionCache}>
        <Component {...pageProps} />
      </CacheProvider>
    </ErrorBoundary>
  );
};

export default withApollo(MyApp);
