import { BaseLayout, PageLayout } from '@app/layouts';

export async function getServerSideProps({ _req, query }) {
  return { props: { query } };
}

const Balance = ({ query }) => {
  const { id } = query;

  return (
    <BaseLayout title={`Balance: ${id}`}>
      <PageLayout>
        <div>
          <h1>Balance: {id}</h1>
        </div>
      </PageLayout>
    </BaseLayout>
  );
};

export default Balance;
