import { BaseLayout, PageLayout } from '@app/layouts';
import { BalancesPage } from '@app/views';

const Balances = () => {
  return (
    <BaseLayout title="Balances">
      <PageLayout>
        <BalancesPage />
      </PageLayout>
    </BaseLayout>
  );
};

export default Balances;
