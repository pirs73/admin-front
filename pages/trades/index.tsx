import { BaseLayout, PageLayout } from '@app/layouts';
import { TradesPage } from '@app/views';

const Trades = () => {
  return (
    <BaseLayout title="Trades">
      <PageLayout>
        <TradesPage />
      </PageLayout>
    </BaseLayout>
  );
};

export default Trades;
