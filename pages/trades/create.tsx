import { BaseLayout, PageLayout } from '@app/layouts';

const CreateTrade = () => {
  return (
    <BaseLayout title="Create Trade">
      <PageLayout>
        <div>
          <h1>Create Trade</h1>
        </div>
      </PageLayout>
    </BaseLayout>
  );
};

export default CreateTrade;
