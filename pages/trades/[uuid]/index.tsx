import { BaseLayout, PageLayout } from '@app/layouts';

export async function getServerSideProps({ _req, query }) {
  return { props: { query } };
}

const Trade = ({ query }) => {
  const { uuid } = query;

  return (
    <BaseLayout title={`Trade: ${uuid}`}>
      <PageLayout>
        <div>
          <h1>Trade # {uuid}</h1>
        </div>
      </PageLayout>
    </BaseLayout>
  );
};

export default Trade;
