import { BaseLayout, PageLayout } from '@app/layouts';

export async function getServerSideProps({ _req, query }) {
  return { props: { query } };
}

const EditTrade = ({ query }) => {
  const { uuid } = query;

  return (
    <BaseLayout title={`Edit Trade: ${uuid}`}>
      <PageLayout>
        <div>
          <h1>Edit Trade # {uuid}</h1>
        </div>
      </PageLayout>
    </BaseLayout>
  );
};

export default EditTrade;
