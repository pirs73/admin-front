import { useEffect } from 'react';
import { useRouter } from 'next/navigation';
import { BaseLayout, PageLayout } from '@app/layouts';

const Index = () => {
  const router = useRouter();

  useEffect(() => {
    router.push('/ads');
  }, [router]);

  return (
    <BaseLayout>
      <PageLayout>&npsb;</PageLayout>
    </BaseLayout>
  );
};

export default Index;
