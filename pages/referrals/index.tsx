import { BaseLayout, PageLayout } from '@app/layouts';
import { ReferralsPage } from '@app/views';

const Referrals = () => {
  return (
    <BaseLayout title="Referrals">
      <PageLayout>
        <ReferralsPage />
      </PageLayout>
    </BaseLayout>
  );
};

export default Referrals;
