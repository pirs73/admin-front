import { BaseLayout, PageLayout } from '@app/layouts';

const CreateReferral = () => {
  return (
    <BaseLayout title="Create Referral">
      <PageLayout>
        <div>
          <h1>Create Referral</h1>
        </div>
      </PageLayout>
    </BaseLayout>
  );
};

export default CreateReferral;
