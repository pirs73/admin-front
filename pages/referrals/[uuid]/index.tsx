import { BaseLayout, PageLayout } from '@app/layouts';

export async function getServerSideProps({ _req, query }) {
  return { props: { query } };
}

const Referral = ({ query }) => {
  const { uuid } = query;

  return (
    <BaseLayout title={`Referral: ${uuid}`}>
      <PageLayout>
        <div>
          <h1>Referral # {uuid}</h1>
        </div>
      </PageLayout>
    </BaseLayout>
  );
};

export default Referral;
