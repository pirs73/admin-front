import { BaseLayout, PageLayout } from '@app/layouts';
import { LogsPage } from '@app/views';

const Logs = () => {
  return (
    <BaseLayout title="Logs">
      <PageLayout>
        <LogsPage />
      </PageLayout>
    </BaseLayout>
  );
};

export default Logs;
