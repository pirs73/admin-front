import { BaseLayout, PageLayout } from '@app/layouts';

const CreateCashback = () => {
  return (
    <BaseLayout title="Create Cashback">
      <PageLayout>
        <div>
          <h1>Create Cashback</h1>
        </div>
      </PageLayout>
    </BaseLayout>
  );
};

export default CreateCashback;
