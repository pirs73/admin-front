import { BaseLayout, PageLayout } from '@app/layouts';

export async function getServerSideProps({ _req, query }) {
  return { props: { query } };
}

const EditCashback = ({ query }) => {
  const { id } = query;

  return (
    <BaseLayout title={`Edit Cashback: ${id}`}>
      <PageLayout>
        <div>
          <h1>Edit Cashback # {id}</h1>
        </div>
      </PageLayout>
    </BaseLayout>
  );
};

export default EditCashback;
