import { BaseLayout, PageLayout } from '@app/layouts';

export async function getServerSideProps({ _req, query }) {
  return { props: { query } };
}

const Cashback = ({ query }) => {
  const { id } = query;

  return (
    <BaseLayout title={`Cashback: ${id}`}>
      <PageLayout>
        <div>
          <h1>Cashback: {id}</h1>
        </div>
      </PageLayout>
    </BaseLayout>
  );
};

export default Cashback;
