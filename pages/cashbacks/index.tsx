import { BaseLayout, PageLayout } from '@app/layouts';
import { CashbacksPage } from '@app/views';

const Cashbacks = () => {
  return (
    <BaseLayout title="Cashbacks">
      <PageLayout>
        <CashbacksPage />
      </PageLayout>
    </BaseLayout>
  );
};

export default Cashbacks;
