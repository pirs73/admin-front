import { BaseLayout, PageLayout } from '@app/layouts';
import { TransactionsPage } from '@app/views';

const Transactions = () => {
  return (
    <BaseLayout title="Transactions">
      <PageLayout>
        <TransactionsPage />
      </PageLayout>
    </BaseLayout>
  );
};

export default Transactions;
