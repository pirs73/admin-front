import { BaseLayout, PageLayout } from '@app/layouts';

const CreateTransaction = () => {
  return (
    <BaseLayout title="Create Transaction">
      <PageLayout>
        <div>
          <h1>Create Transaction</h1>
        </div>
      </PageLayout>
    </BaseLayout>
  );
};

export default CreateTransaction;
