import { BaseLayout, PageLayout } from '@app/layouts';

export async function getServerSideProps({ _req, query }) {
  return { props: { query } };
}

const EditTransaction = ({ query }) => {
  const { uuid } = query;

  return (
    <BaseLayout title={`Edit Transaction: ${uuid}`}>
      <PageLayout>
        <div>
          <h1>Edit Transaction # {uuid}</h1>
        </div>
      </PageLayout>
    </BaseLayout>
  );
};

export default EditTransaction;
