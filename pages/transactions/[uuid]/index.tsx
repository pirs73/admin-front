import { BaseLayout, PageLayout } from '@app/layouts';

export async function getServerSideProps({ _req, query }) {
  return { props: { query } };
}

const Transaction = ({ query }) => {
  const { uuid } = query;

  return (
    <BaseLayout title={`Transaction: ${uuid}`}>
      <PageLayout>
        <div>
          <h1>Transaction # {uuid}</h1>
        </div>
      </PageLayout>
    </BaseLayout>
  );
};

export default Transaction;
