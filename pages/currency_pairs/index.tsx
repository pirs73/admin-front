import { BaseLayout, PageLayout } from '@app/layouts';
import { CurrencyPairsPage } from '@app/views';

const CurrencyPairs = () => {
  return (
    <BaseLayout title="CurrencyPairs">
      <PageLayout>
        <CurrencyPairsPage />
      </PageLayout>
    </BaseLayout>
  );
};

export default CurrencyPairs;
