import { BaseLayout, PageLayout } from '@app/layouts';

export async function getServerSideProps({ _req, query }) {
  return { props: { query } };
}

const CurrencyPair = ({ query }) => {
  const { name } = query;

  return (
    <BaseLayout title={`Currency Pair: ${name}`}>
      <PageLayout>
        <div>
          <h1>Currency Pair: {name}</h1>
        </div>
      </PageLayout>
    </BaseLayout>
  );
};

export default CurrencyPair;
