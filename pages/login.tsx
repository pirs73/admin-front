import { BaseLayout } from '@app/layouts';
import { LoginPage } from '@app/views';

function Login() {
  return (
    <BaseLayout title="Sign In">
      <LoginPage />
    </BaseLayout>
  );
}

export default Login;
