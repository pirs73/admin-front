import { BaseLayout, PageLayout } from '@app/layouts';

const CreateWallet = () => {
  return (
    <BaseLayout title="Create Wallet">
      <PageLayout>
        <div>
          <h1>Create Wallet</h1>
        </div>
      </PageLayout>
    </BaseLayout>
  );
};

export default CreateWallet;
