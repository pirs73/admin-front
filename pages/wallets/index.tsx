import { BaseLayout, PageLayout } from '@app/layouts';
import { WalletsPage } from '@app/views';

const Wallets = () => {
  return (
    <BaseLayout title="Wallets">
      <PageLayout>
        <WalletsPage />
      </PageLayout>
    </BaseLayout>
  );
};

export default Wallets;
