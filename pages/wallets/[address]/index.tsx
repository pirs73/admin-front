import { BaseLayout, PageLayout } from '@app/layouts';

export async function getServerSideProps({ _req, query }) {
  return { props: { query } };
}

const Wallet = ({ query }) => {
  const { address } = query;

  return (
    <BaseLayout title={`Wallet: ${address}`}>
      <PageLayout>
        <div>
          <h1>Wallet # {address}</h1>
        </div>
      </PageLayout>
    </BaseLayout>
  );
};

export default Wallet;
