import { BaseLayout, PageLayout } from '@app/layouts';

export async function getServerSideProps({ _req, query }) {
  return { props: { query } };
}

const AdDetailsItem = ({ query }) => {
  const { uuid } = query;

  return (
    <BaseLayout title={`Ad detail: ${uuid}`}>
      <PageLayout>
        <div>
          <h1>Ad detail # {uuid}</h1>
        </div>
      </PageLayout>
    </BaseLayout>
  );
};

export default AdDetailsItem;
