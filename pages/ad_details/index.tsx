import { BaseLayout, PageLayout } from '@app/layouts';
import { AdDetailsPage } from '@app/views';

const AdDetails = () => {
  return (
    <BaseLayout title="Ads Details">
      <PageLayout>
        <AdDetailsPage />
      </PageLayout>
    </BaseLayout>
  );
};

export default AdDetails;
