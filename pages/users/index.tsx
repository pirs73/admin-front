import { BaseLayout, PageLayout } from '@app/layouts';
import { UsersPage } from '@app/views';

const Users = () => {
  return (
    <BaseLayout title="Users">
      <PageLayout>
        <UsersPage />
      </PageLayout>
    </BaseLayout>
  );
};

export default Users;
