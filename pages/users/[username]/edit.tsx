import { BaseLayout, PageLayout } from '@app/layouts';

export async function getServerSideProps({ _req, query }) {
  return { props: { query } };
}

const EditUser = ({ query }) => {
  const { username } = query;

  return (
    <BaseLayout title={`Edit User: ${username}`}>
      <PageLayout>
        <div>
          <h1>Edit User # {username}</h1>
        </div>
      </PageLayout>
    </BaseLayout>
  );
};

export default EditUser;
