import { BaseLayout, PageLayout } from '@app/layouts';

export async function getServerSideProps({ _req, query }) {
  return { props: { query } };
}

const User = ({ query }) => {
  const { username } = query;

  return (
    <BaseLayout title={`User: ${username}`}>
      <PageLayout>
        <div>
          <h1>User: {username}</h1>
        </div>
      </PageLayout>
    </BaseLayout>
  );
};

export default User;
