import { BaseLayout, PageLayout } from '@app/layouts';
import { AdBanksPage } from '@app/views';

const AdBanks = () => {
  return (
    <BaseLayout title="Ad Banks">
      <PageLayout>
        <AdBanksPage />
      </PageLayout>
    </BaseLayout>
  );
};

export default AdBanks;
