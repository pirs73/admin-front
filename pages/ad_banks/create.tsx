import { BaseLayout, PageLayout } from '@app/layouts';

const CreateAdBank = () => {
  return (
    <BaseLayout title="Create Ad Bank">
      <PageLayout>
        <div>
          <h1>Create Ad Bank</h1>
        </div>
      </PageLayout>
    </BaseLayout>
  );
};

export default CreateAdBank;
