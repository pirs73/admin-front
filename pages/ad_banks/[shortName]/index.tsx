import { BaseLayout, PageLayout } from '@app/layouts';

export async function getServerSideProps({ _req, query }) {
  return { props: { query } };
}

const AdBank = ({ query }) => {
  const { shortName } = query;

  return (
    <BaseLayout title={`Ad Bank: ${shortName}`}>
      <PageLayout>
        <div>
          <h1>Ad Bank {shortName}</h1>
        </div>
      </PageLayout>
    </BaseLayout>
  );
};

export default AdBank;
