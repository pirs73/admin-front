import { BaseLayout, PageLayout } from '@app/layouts';

export async function getServerSideProps({ _req, query }) {
  return { props: { query } };
}

const EditAdBank = ({ query }) => {
  const { shortName } = query;

  return (
    <BaseLayout title={`Edit Ad Bank: ${shortName}`}>
      <PageLayout>
        <div>
          <h1>Edit Ad Bank # {shortName}</h1>
        </div>
      </PageLayout>
    </BaseLayout>
  );
};

export default EditAdBank;
