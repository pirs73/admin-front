import withApollo from '@app/hoc/withApollo';
import { BaseLayout } from '@app/layouts';
import { RegisterPage } from '@app/views';

function SignUp() {
  return (
    <BaseLayout title="Sign Up">
      <RegisterPage />
    </BaseLayout>
  );
}

export default withApollo(SignUp);
