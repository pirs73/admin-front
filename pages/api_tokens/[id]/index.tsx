import { BaseLayout, PageLayout } from '@app/layouts';

export async function getServerSideProps({ _req, query }) {
  return { props: { query } };
}

const ApiToken = ({ query }) => {
  const { id } = query;

  return (
    <BaseLayout title={`ApiToken: ${id}`}>
      <PageLayout>
        <div>
          <h1>ApiToken # {id}</h1>
        </div>
      </PageLayout>
    </BaseLayout>
  );
};

export default ApiToken;
