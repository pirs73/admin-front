import { BaseLayout, PageLayout } from '@app/layouts';

const CreateApiToken = () => {
  return (
    <BaseLayout title="Create ApiToken">
      <PageLayout>
        <div>
          <h1>Create ApiToken</h1>
        </div>
      </PageLayout>
    </BaseLayout>
  );
};

export default CreateApiToken;
