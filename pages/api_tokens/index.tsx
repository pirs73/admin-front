import { BaseLayout, PageLayout } from '@app/layouts';
import { ApiTokensPage } from '@app/views';

const ApiTokens = () => {
  return (
    <BaseLayout title="Api Tokens">
      <PageLayout>
        <ApiTokensPage />
      </PageLayout>
    </BaseLayout>
  );
};

export default ApiTokens;
