import { useEffect } from 'react';
import { useRouter } from 'next/router';
import { gql, useMutation, useApolloClient } from '@apollo/client';

const SignOutMutation = gql`
  mutation SignOutMutation {
    signOut
  }
`;

function SignOut() {
  const client = useApolloClient();
  const router = useRouter();
  const [signOut] = useMutation(SignOutMutation);

  useEffect(() => {
    async function logout() {
      try {
        const { data, error } = await signOut();
        if (error) {
          console.error(error);
        }
        if (data?.signOut) {
          await client.resetStore();
          router.push('/login');
        }
      } catch (error) {
        console.error(error);
      }
    }

    logout();
  }, [signOut, router, client]);

  return <p>Signing out...</p>;
}

export default SignOut;
