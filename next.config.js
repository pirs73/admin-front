// const path = require('path');
// const { TsconfigPathsPlugin } = require('tsconfig-paths-webpack-plugin');
const withBundleAnalyzer = require('@next/bundle-analyzer')({
  enabled: process.env.ANALYZE === 'true',
});

module.exports = withBundleAnalyzer();

module.exports = {
  webpack(config) {
    // new TsconfigPathsPlugin({
    //   configFile: path.resolve(__dirname, './tsconfig.json'),
    // });
    config.module.rules.push({
      test: /\.svg$/,
      use: ['@svgr/webpack'],
    });

    return config;
  },
  reactStrictMode: false,
  poweredByHeader: false,
  swcMinify: true,
};
